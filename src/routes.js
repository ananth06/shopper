import React from 'react';
import DefaultLayout from './containers/DefaultLayout';

const Dashboard = React.lazy(() => import('./views/Dashboard'));
const Orders = React.lazy(() => import('./views/Orders/Orders'));
const OrderDetails = React.lazy(() => import('./views/Orders/OrderDetails'));
const Login = React.lazy(() => import('./views/Pages/Login'));


// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home', component: DefaultLayout },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/orders', exact: true,  name: 'Orders', component: Orders },
  { path: '/orderdetails', exact: true,  name: 'OrderDetails', component: OrderDetails },
  { path: '/login', exact: true,  name: 'Login', component: Login },
];

export default routes;
