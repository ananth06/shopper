import React, { Component } from 'react';
import { Col, Nav, NavItem, NavLink, Row, TabContent, TabPane, Table, Button, InputGroup, Input } from 'reactstrap';
import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { withRouter } from 'react-router-dom';
import Moment from 'react-moment';
import moment from 'moment-timezone';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import OrderDetails from './OrderDetails';
import CustomerShopDetails from './CustomerShopDetails';
import Loader from 'react-loader-spinner';


class Orders extends Component {

  authToken = '';
  orderdetail = '';
  loader = '';
  customerShopDeliveryContactDetails = '';
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.orderDetailPage = this.orderDetailPage.bind(this);
    this.addActiveClass = this.addActiveClass.bind(this);
    this.state = {
      authToken: '',
      activeTab: new Array(4).fill('1'),
      dropdownOpen: false,
      status: 1,
      count: 0,
      orderdetail: '',
      customerShopDeliveryContactDetails: '',
      orderStatus: [
        '',
        'newOrders',
        'acceptedOrder',
        'readyToShipOrders',
        'shippingOrders',
        'deliveredOrders',
        'Verified',
        'cancelledOrders',
        'RejectedByShop',
        'escalatedOrders'
      ],
      orderListData: [
      ],

      orderListUnReadCount: {
        'newOrders': 0,
        'acceptedOrder': 0,
        'readyToShipOrders': 0,
        'shippingOrders': 0,
        'deliveredOrders': 0,
        'Verified': 0,
        'cancelledOrders': 0,
        'RejectedByShop': 0,
        'escalatedOrders': 0
      },

      orderListCount: {
        'newOrders': 0,
        'acceptedOrder': 0,
        'readyToShipOrders': 0,
        'shippingOrders': 0,
        'deliveredOrders': 0,
        'Verified': 0,
        'cancelledOrders': 0,
        'RejectedByShop': 0,
        'escalatedOrders': 0
      },
      shopContactDetails: '',
      customerContactDetails: '',
      deliveryContactDetails: '',
      customerNameFilter: '',
      deliveryLocationFilter: '',
      typeFilter: '',
      orderTypeFilter: '',
      daysFilter: '',
      date1Filter: null,
      date2Filter: null,
      activeDays: ''
    }

    let userData = localStorage.getItem('userData');
    if (userData !== undefined && userData !== null) {
      let authStr = JSON.parse(userData);
      this.authToken = authStr.auth_token;
      this.getStatusCount('');
    }
    this.showTabPane = this.showTabPane.bind(this)
    this.showOrderList = this.showOrderList.bind(this)
  }

  componentDidMount() {
  }

  showTabPane(value) {
    if (value !== undefined) {
      this.setState({ orderdetail: '', customerShopDeliveryContactDetails: '' })
    }
    if ((this.state.orderdetail !== undefined && this.state.orderdetail === '') || (value !== undefined && value === false)) {
      //  this.getOrders(1, null, 'newOrder');
      return this.tabPane()
    }
  }

  showOrderList() {    
    if ((this.state.orderdetail !== undefined && this.state.orderdetail !== '')) {
      if(this.state.activeTab[0] === '3'){
        return this.toggle(0, '3', 3, 'readyToShipOrders', this.state.orderListCount.readyToShipOrders);
      }
      if(this.state.activeTab[0] === '4'){
        return this.toggle(0, '4', 4, 'shippingOrders', this.state.orderListCount.shippingOrders);
      }      
    }
  }

  addActiveClass(noOfDays) {
    this.setState({
      daysFilter: noOfDays,
      activeDays: noOfDays
    });
  }

  getStatusCount(getOrderCallStatus) {
    if (this.authToken !== '') {
      this.loader = <div className="cutomLoader"> <Loader type="Oval" color="#00BFFF" height="100" width="100" /> </div>;
      axios.get(process.env.REACT_APP_API_BASE_URL + 'order/unread/count', { 'headers': { 'Authorization': 'JWT ' + this.authToken } }).then((response) => {
        var stateCopy = Object.assign({}, this.state);
        for (let i = 1; i <= this.state.orderStatus.length; i++) {
          if (response.data.data[this.state.orderStatus[i]] !== undefined) {
            stateCopy.orderListCount[this.state.orderStatus[i]] = response.data.data[this.state.orderStatus[i]].total;
            stateCopy.orderListUnReadCount[this.state.orderStatus[i]] = response.data.data[this.state.orderStatus[i]].unread;
          } else {
            stateCopy.orderListCount[this.state.orderStatus[i]] = 0;
            stateCopy.orderListUnReadCount[this.state.orderStatus[i]] = 0;
          }
        }
        this.setState(stateCopy);

        // initial call for first tab with status "New Order"
        if (getOrderCallStatus !== false) {
          this.getOrders(1, '', 'newOrder', this.state.orderListCount.newOrders);
          this.tabClick(1, '', 'newOrder', '', this.state.orderListCount.newOrders);
        }
      }, error => {
        return error;
      });
      this.loader = '';
    }
    // initial call for first tab with status "New Order"
    // if (getOrderCallStatus !== false) {
    //   setTimeout(() => {
    //     alert(this.state.orderListCount.newOrders);
    //   }, 100);
    //   this.getOrders(1, '', 'newOrder', this.state.orderListCount.newOrders);
    //   this.tabClick(1, '', 'newOrder', '', this.state.orderListCount.newOrders);
    // }
  }

  toggles() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }


  async toggle(tabPane, tab, statustab, apiname, limitCount) {
    const newArray = this.state.activeTab.slice()
    newArray[tabPane] = tab
    await this.setState({
      activeTab: newArray,
      orderdetail: '',
      customerShopDeliveryContactDetails: '',
      customerNameFilter: '',
      deliveryLocationFilter: '',
      typeFilter: '',
      orderTypeFilter: '',
      daysFilter: '',
      date1Filter: '',
      date2Filter: '',
      activeDays: ''
    });
    this.getOrders(statustab, '', apiname, limitCount);
    this.tabClick(statustab, '', apiname, 'reset', limitCount);
  }

  tabClick(status, event, apiName, resetForm, limitCount) {
    clearInterval(this.interval);
    let timeoutSec = 120000;
    if (parseInt(localStorage.getItem('setActiveTab')) === 1) {
      timeoutSec = 60000;
    }
    this.setState({
      customerNameFilter: '',
      deliveryLocationFilter: '',
      typeFilter: '',
      orderTypeFilter: '',
      daysFilter: '',
      date1Filter: '',
      date2Filter: '',
      activeDays: ''
    });

    if (resetForm !== '') {
      this.orderForm1.reset();
      this.orderForm2.reset();
      this.orderForm3.reset();
      this.orderForm4.reset();
      this.orderForm5.reset();
      this.orderForm6.reset();
      this.orderForm7.reset();
    }
    this.interval = setInterval(() => {
      this.getOrders(status, event, apiName, limitCount);
    }, timeoutSec);
  }

  getOrders(status, event, apiName, limitCount) {
    this.props.headerClassmessage('dashboard');
    localStorage.setItem('setActiveTab', status);
    localStorage.setItem('setActiveAPI', apiName);
    if (event !== '' && event !== null && event !== undefined) {
      event.preventDefault();
    }



    if (this.authToken !== '') {
      var selectedDate = '';
      var selectedDate2 = '';

      if (this.state.date1Filter !== null && this.state.date1Filter !== '') {
        selectedDate = this.state.date1Filter;
        selectedDate = moment(selectedDate).utc().format("YYYY-MM-DD[T]HH:mm:ss.SSSZZ")
        selectedDate = selectedDate.toLocaleString();
        selectedDate = moment(selectedDate).format("YYYY-MM-DD[T]HH:mm:ss.SSS")
      }
      if (this.state.date2Filter !== null && this.state.date2Filter !== '') {
        selectedDate2 = this.state.date2Filter;
        selectedDate2 = moment(selectedDate2).utc().format("YYYY-MM-DD[T]HH:mm:ss.SSSZZ")
        selectedDate2 = selectedDate2.toLocaleString();
        selectedDate2 = moment(selectedDate2).format("YYYY-MM-DD[T]HH:mm:ss.SSS")
        selectedDate2 = selectedDate2.replace("00:00:00.000", "23:59:59.999");
      }

      this.loader = <div className="cutomLoader"> <Loader type="Oval" color="#00BFFF" height="100" width="100" /> </div>;
      axios.get(process.env.REACT_APP_API_BASE_URL + 'order/' + apiName + '?skip=0&limit=' + limitCount + '&text=' + this.state.customerNameFilter + '&type=' + this.state.typeFilter + '&orderType=' + this.state.orderTypeFilter + '&location=' + this.state.deliveryLocationFilter + '&date1=' + selectedDate + '&date2=' + selectedDate2 + '&days=' + this.state.daysFilter, { 'headers': { 'Authorization': 'JWT ' + this.authToken } }).then((response) => {

        // to call Read and Unread Count API        
        this.getStatusCount(false);
        for (var i = 0; i < response.data.data.length; i++) {
          // if Distance is Take Away then display as '-'
          if (typeof response.data.data[i].distance === 'string' || response.data.data[i].distance instanceof String) {
            response.data.data[i].distance = '-';
          }

          response.data.data[i].read_receipt_status = true;
          /* For New order Tab */
          if (apiName === 'newOrder') {
            let orderReadReceiptStatus = true;
            if (response.data.data[i].ordered === undefined) {
              response.data.data[i].ordered.read_receipt = false;
              response.data.data[i].read_receipt_status = false;
              orderReadReceiptStatus = false;
            }
            if (orderReadReceiptStatus) {
              if (response.data.data[i].ordered.read_receipt === undefined) {
                response.data.data[i].ordered.read_receipt = false;
                response.data.data[i].read_receipt_status = false;
              }
            }
          }

          /* For Packaging Tab */
          if (apiName === 'acceptedOrder') {
            let readReceiptStatus = true;
            if (response.data.data[i].accepted === undefined) {
              response.data.data[i].read_receipt_status = false;
              readReceiptStatus = false;
            }
            if (readReceiptStatus) {
              if (response.data.data[i].accepted.read_receipt === undefined) {
                response.data.data[i].read_receipt_status = false;
              }
            }
          }

          /* For Dispatched Tab */
          if (apiName === 'readyToShipOrders') {
            let shipOrderReceiptStatus = true;
            if (response.data.data[i].ready_to_ship === undefined) {
              response.data.data[i].read_receipt_status = false;
              shipOrderReceiptStatus = false;
            }
            if (shipOrderReceiptStatus) {
              if (response.data.data[i].ready_to_ship.read_receipt === undefined) {
                response.data.data[i].read_receipt_status = false;
              }
            }
          }

          /* For Shipping Tab */
          if (apiName === 'shippingOrders') {
            let shippingOrderReceiptStatus = true;
            if (response.data.data[i].shipping === undefined) {
              response.data.data[i].read_receipt_status = false;
              shippingOrderReceiptStatus = false;
            }
            if (shippingOrderReceiptStatus) {
              if (response.data.data[i].shipping.read_receipt === undefined) {
                response.data.data[i].read_receipt_status = false;
              }
            }
          }

          /* For Delivered Tab */
          if (apiName === 'deliveredOrders') {
            if (response.data.data[i].status === 5) {
              let deliveredReceiptStatus = true;
              if (response.data.data[i].delivered === undefined) {
                response.data.data[i].read_receipt_status = false;
                deliveredReceiptStatus = false;
              }
              if (deliveredReceiptStatus) {
                if (response.data.data[i].delivered.read_receipt === undefined) {
                  response.data.data[i].read_receipt_status = false;
                }
              }
            }

            if (response.data.data[i].status === 6) {
              let verifiedReceiptStatus = true;
              if (response.data.data[i].verified === undefined) {
                response.data.data[i].read_receipt_status = false;
                verifiedReceiptStatus = false;
              }
              if (verifiedReceiptStatus) {
                if (response.data.data[i].verified.read_receipt === undefined) {
                  response.data.data[i].read_receipt_status = false;
                }
              }
            }
          }

          /* For Cancelled Tab */
          if (apiName === 'cancelledOrders') {
            if (response.data.data[i].status === 7) {
              let cancelledReceiptStatus = true;
              if (response.data.data[i].cancelled === undefined) {
                response.data.data[i].read_receipt_status = false;
                cancelledReceiptStatus = false;
              }
              if (cancelledReceiptStatus) {
                if (response.data.data[i].cancelled.read_receipt === undefined) {
                  response.data.data[i].read_receipt_status = false;
                }
              }
            }

            if (response.data.data[i].status === 8) {
              if (response.data.data[i].rejected !== undefined) {
                if (response.data.data[i].rejected.read_receipt === undefined) {
                  response.data.data[i].read_receipt_status = false;
                }
              }
            }

            if (response.data.data[i].status === 9) {
              if (response.data.data[i].denied !== undefined) {
                if (response.data.data[i].denied.read_receipt === undefined) {
                  response.data.data[i].read_receipt_status = false;
                }
              }
            }
          }

          /* For Escalated Tab */
          if (apiName === 'escalatedOrders') {
            let verifiedReceiptStatus = true;
            if (response.data.data[i].verified === undefined) {
              response.data.data[i].verified.read_receipt = false;
              response.data.data[i].read_receipt_status = false;
              verifiedReceiptStatus = false;
            }
            if (verifiedReceiptStatus) {
              if (response.data.data[i].verified.read_receipt === undefined) {
                response.data.data[i].verified.read_receipt = false;
                response.data.data[i].read_receipt_status = false;
              }
            }
          }
        }
        this.setState({
          orderListData: response.data.data,
        });
        this.loader = '';

      }, error => {
        this.loader = '';
        if (error.response.status === 401) {
          localStorage.removeItem("userData");
          toast.error(error.response.data.message);
        } else if (error.response.status === 500) {
          this.setState({
            orderListData: [],
          });
        } else {

        }
        return error;
      });

    }
  }

  orderDetailPage(orderId, activeTab, orderCategory, productOrderId, tabName, arrayIndex) {
    this.props.headerClassmessage('orders');
    this.loader = <div className="cutomLoader"> <Loader type="Oval" color="#00BFFF" height="100" width="100" /> </div>;
    axios.get(process.env.REACT_APP_API_BASE_URL + 'order/' + orderId + '/info', { 'headers': { 'Authorization': 'JWT ' + this.authToken } }).then((response) => {
      if (response.data !== '') {
        // to call Read and Unread Count API
        this.getStatusCount(false);

        this.setState({
          orderDetails: response.data.data
        });

        axios.get(process.env.REACT_APP_API_BASE_URL + 'order/' + orderId + '/support/contact/details', { 'headers': { 'Authorization': 'JWT ' + this.authToken } }).then((response) => {

          if (response.data !== '') {
            /* Status from Unread to Read Change when user link to Order Detail Page */
            var stateCopy = Object.assign({}, this.state);
            stateCopy.orderListData[arrayIndex].read_receipt_status = true;
            this.setState(stateCopy);

            /** For Escalation Tab the parameter has_escalation is present and has_escalation present status is passed into product detail page */
            let has_escalation_status = false;
            for (let i = 0; i < this.state.orderListData.length; i++) {
              if (this.state.orderListData[i].has_escalation !== undefined) {
                has_escalation_status = true;
              }
            }
            this.setState({
              shopContactDetails: response.data.data.shop,
              customerContactDetails: response.data.data.customer,
              deliveryContactDetails: response.data.data.delivery,
              orderdetail: <OrderDetails currentOrderDetail={stateCopy.orderListData[arrayIndex]} hasEscalationStatus={has_escalation_status} tabName={tabName} orderCategory={orderCategory} orderId={productOrderId} orderDetails={this.state.orderDetails} customerDetails={response.data} backButton={this.showTabPane} orderListPage={this.showOrderList} parentState={this.state} />,
              customerShopDeliveryContactDetails: <CustomerShopDetails shopContactDetails={response.data.data.shop} customerContactDetails={response.data.data.customer} deliveryContactDetails={response.data.data.delivery} />
            });
            this.loader = '';
          }
        }, error => {
          return error;
        });
      }
    }, error => {
      return error;
    });
  }

  tabPane() {
    let noRecordFound = <tr><td className='p-0' colSpan="11"></td></tr>;
    if (this.state.orderListData.length === 0) {
      noRecordFound = <tr><td colSpan="11"><div>No Records Found</div></td></tr>
    }

    return (
      <>
        <TabPane tabId="1">
          <div>
            <form ref={(el) => this.orderForm1 = el} onSubmit={(e) => this.getOrders(1, e, 'newOrder', this.state.orderListCount.newOrders)}>
              <div className="col-sm-12 d-flex flex-column flex-md-row align-items-center mb-2 tableHeader px-0">

                <div className="mr-3 mb-3 mb-md-0">
                  <InputGroup className="d-flex flex-row align-items-center position-relative searchBox">
                    <Input className="pl-4" onKeyUp={e => {
                      this.setState({
                        customerNameFilter: e.target.value
                      })
                    }} placeholder="Customer name, Order Id" />
                  </InputGroup>
                </div>
                <div className="d-flex align-items-center flex-row mb-3 mb-md-0 mr-md-4">
                  <span className="pr-2">Delivery Location</span>
                  <select onChange={e => {
                    this.setState({
                      deliveryLocationFilter: e.target.value
                    })
                  }}>
                    <option value="All">All</option>
                  </select>
                </div>
                <div className="d-flex align-items-center flex-row mb-3 mb-md-0 mr-md-4">
                  <span className="pr-2">Filter</span>
                  <select onChange={e => {
                    this.setState({
                      typeFilter: e.target.value
                    })
                  }}>
                    <option value="">All</option>
                    <option value="read">Read</option>
                    <option value="unread">Unread</option>
                  </select>
                </div>
                <div className="d-flex align-items-center flex-row">
                  <span className="pr-2">Order Type</span>
                  <select onChange={e => {
                    this.setState({
                      orderTypeFilter: e.target.value
                    })
                  }}>
                    <option value="">All</option>
                    <option value="local">Local</option>
                    <option value="other">Other</option>
                  </select>
                </div>

                <div className="d-flex align-items-center flex-row headerGoButton">
                  <button className="btn btn-flat btn-preview-addcard pull-right border py-1 px-2 ml-2" onSubmit={(e) => this.getOrders(1, e, 'newOrder', this.state.orderListCount.newOrders)}>
                    GO
                  </button>
                </div>
              </div>
            </form>


            <Table id="customTable" className="table table-striped table-bordered customTable orderTable" responsive>
              <thead>
                <tr>
                  <th>Elapsed Time</th>
                  <th>Order ID</th>
                  <th>Order Date & Time</th>
                  <th>Support Status</th>
                  <th>Shop Category</th>
                  <th>Customer Name</th>
                  <th>Shop Name</th>
                  <th>Distance</th>
                  <th>Product Count</th>
                  <th>Purchase Amount</th>
                  <th>Payment mode</th>
                </tr>
              </thead>
              <tbody>
                {noRecordFound}
                {this.state.orderListData.map((data, index) =>
                  <tr key={index} onClick={() => { this.orderDetailPage(data._id, 1, data.category, data.order_id, 'New Order', index) }}>
                    <td>
                      <Moment interval={1} duration={data.ordered.at}
                        durationFromNow />
                    </td>
                    <td>{data.order_id}</td>
                    <td>
                      <Moment format="D MMM YYYY - h.mm a">{data.ordered.at}</Moment>
                    </td>
                    <td>{data.read_receipt_status !== false ? <Button color="primary">Read</Button > : <Button color="danger">Unread</Button >} </td>
                    <td>{data.category}</td>
                    <td>{data.customer.name}</td>
                    <td>{data.store.display_name}</td>
                    <td>{data.distance}</td>
                    <td>{data.product_count}</td>
                    <td>{data.prices.net_price}</td>
                    <td>{data.payment_type}</td>
                  </tr>
                )}
              </tbody>
            </Table>
          </div>

          {/* Order Detail Page */}
        </TabPane>

        <TabPane tabId="2">
          <div>
            <form ref={(el) => this.orderForm2 = el} onSubmit={(e) => this.getOrders(2, e, 'acceptedOrder', this.state.orderListCount.acceptedOrder)}>
              <div className="col-sm-12 d-flex flex-column flex-md-row align-items-center mb-2 tableHeader px-0">
                <div className="mr-3 mb-3 mb-md-0">
                  <InputGroup className="d-flex flex-row align-items-center position-relative searchBox">
                    <Input className="pl-4" onKeyUp={e => {
                      this.setState({
                        customerNameFilter: e.target.value
                      })
                    }} placeholder="Customer name, Order Id" />
                  </InputGroup>
                </div>
                <div className="d-flex align-items-center flex-row mb-3 mb-md-0 mr-md-4">
                  <span className="pr-2">Delivery Location</span>
                  <select onChange={e => {
                    this.setState({
                      deliveryLocationFilter: e.target.value
                    })
                  }}>
                    <option value="All">All</option>
                  </select>
                </div>
                <div className="d-flex align-items-center flex-row mb-3 mb-md-0 mr-md-4">
                  <span className="pr-2">Filter</span>
                  <select onChange={e => {
                    this.setState({
                      typeFilter: e.target.value
                    })
                  }}>
                    <option value="">All</option>
                    <option value="read">Read</option>
                    <option value="unread">Unread</option>
                  </select>
                </div>
                <div className="d-flex align-items-center flex-row">
                  <span className="pr-2">Order Type</span>
                  <select onChange={e => {
                    this.setState({
                      orderTypeFilter: e.target.value
                    })
                  }}>
                    <option value="">All</option>
                    <option value="local">Local</option>
                    <option value="other">Other</option>
                  </select>
                </div>

                <div className="d-flex align-items-center flex-row">
                  <button className="btn btn-flat btn-preview-addcard pull-right border py-1 px-2 ml-2" onSubmit={(e) => this.getOrders(2, e, 'acceptedOrder', this.state.orderListCount.acceptedOrder)}>
                    GO
                  </button>
                </div>
              </div>
            </form>
            <Table id="customTable" className="table table-striped table-bordered customTable orderTable" responsive>
              <thead>
                <tr>
                  <th>Elapsed Time</th>
                  <th>Order ID</th>
                  <th>Order Date & Time</th>
                  <th>Support Status</th>
                  <th>Shop Category</th>
                  <th>Customer Name</th>
                  <th>Shop Name</th>
                  <th>Distance</th>
                  <th>Product Count</th>
                  <th>Purchase Amount</th>
                  <th>Payment mode</th>
                </tr>
              </thead>
              <tbody>
                {noRecordFound}
                {this.state.orderListData.map((data, index) =>
                  <tr key={index} onClick={() => { this.orderDetailPage(data._id, 2, data.category, data.order_id, 'Packaging', index) }}>
                    <td>
                      <Moment interval={1} duration={data.ordered.at}
                        durationFromNow />
                    </td>
                    <td>{data.order_id}</td>
                    <td>
                      <Moment format="D MMM YYYY - h.mm a">{data.ordered.at}</Moment>
                    </td>
                    <td>{data.read_receipt_status !== false ? <Button color="primary">Read</Button > : <Button color="danger">Unread</Button >} </td>
                    <td>{data.category}</td>
                    <td>{data.customer.name}</td>
                    <td>{data.store.display_name}</td>
                    <td>{data.distance}</td>
                    <td>{data.product_count}</td>
                    <td>{data.prices.net_price}</td>
                    <td>{data.payment_type}</td>
                  </tr>
                )}
              </tbody>
            </Table>
          </div>
        </TabPane>

        <TabPane tabId="3">
          <div>
            <form ref={(el) => this.orderForm3 = el} onSubmit={(e) => this.getOrders(3, e, 'readyToShipOrders', this.state.orderListCount.readyToShipOrders)}>
              <div className="col-sm-12 d-flex flex-column flex-md-row align-items-center mb-2 tableHeader px-0">
                <div className="mr-3 mb-3 mb-md-0">
                  <InputGroup className="d-flex flex-row align-items-center position-relative searchBox">
                    <Input className="pl-4" onKeyUp={e => {
                      this.setState({
                        customerNameFilter: e.target.value
                      })
                    }} placeholder="Customer name, Order Id" />
                  </InputGroup>
                </div>
                <div className="d-flex align-items-center flex-row mb-3 mb-md-0 mr-md-4">
                  <span className="pr-2">Delivery Location</span>
                  <select onChange={e => {
                    this.setState({
                      deliveryLocationFilter: e.target.value
                    })
                  }}>
                    <option value="All">All</option>
                  </select>
                </div>
                <div className="d-flex align-items-center flex-row mb-3 mb-md-0 mr-md-4">
                  <span className="pr-2">Filter</span>
                  <select onChange={e => {
                    this.setState({
                      typeFilter: e.target.value
                    })
                  }}>
                    <option value="">All</option>
                    <option value="read">Read</option>
                    <option value="unread">Unread</option>
                  </select>
                </div>
                <div className="d-flex align-items-center flex-row">
                  <span className="pr-2">Order Type</span>
                  <select onChange={e => {
                    this.setState({
                      orderTypeFilter: e.target.value
                    })
                  }}>
                    <option value="">All</option>
                    <option value="local">Local</option>
                    <option value="other">Other</option>
                  </select>
                </div>

                <div className="d-flex align-items-center flex-row">
                  <button className="btn btn-flat btn-preview-addcard pull-right border py-1 px-2 ml-2" onSubmit={(e) => this.getOrders(3, e, 'readyToShipOrders', this.state.orderListCount.readyToShipOrders)}>
                    GO
                  </button>
                </div>
              </div>
            </form>


            <Table id="customTable" className="table table-striped table-bordered customTable orderTable" responsive>
              <thead>
                <tr>
                  <th>Elapsed Time</th>
                  <th>Order ID</th>
                  <th>Order Date & Time</th>
                  <th>Support Status</th>
                  <th>Shop Category</th>
                  <th>Customer Name</th>
                  <th>Shop Name</th>
                  <th>Distance</th>
                  <th>Product Count</th>
                  <th>Purchase Amount</th>
                  <th>Payment mode</th>
                </tr>
              </thead>
              <tbody>
                {noRecordFound}
                {this.state.orderListData.map((data, index) =>
                  <tr key={index} onClick={() => { this.orderDetailPage(data._id, 3, data.category, data.order_id, "Dispatched", index) }}>
                    <td>
                      <Moment interval={1} duration={data.ordered.at}
                        durationFromNow />
                    </td>
                    <td>{data.order_id}</td>
                    <td><Moment format="D MMM YYYY - h.mm a">{data.ordered.at}</Moment></td>
                    <td>{data.read_receipt_status !== false ? <Button color="primary">Read</Button > : <Button color="danger">Unread</Button >} </td>
                    <td>{data.category}</td>
                    <td>{data.customer.name}</td>
                    <td>{data.store.display_name}</td>
                    <td>{data.distance}</td>
                    <td>{data.product_count}</td>
                    <td>{data.prices.net_price}</td>
                    <td>{data.payment_type}</td>
                  </tr>
                )}
              </tbody>
            </Table>
          </div>
        </TabPane>


        <TabPane tabId="4">
          <div>
            <form ref={(el) => this.orderForm4 = el} onSubmit={(e) => this.getOrders(4, e, 'shippingOrders', this.state.orderListCount.shippingOrders)}>
              <div className="col-sm-12 d-flex flex-column flex-md-row align-items-center mb-2 tableHeader px-0">
                <div className="mr-3 mb-3 mb-md-0">
                  <InputGroup className="d-flex flex-row align-items-center position-relative searchBox">
                    <Input className="pl-4" onKeyUp={e => {
                      this.setState({
                        customerNameFilter: e.target.value
                      })
                    }} placeholder="Customer name, Order Id" />
                  </InputGroup>
                </div>
                <div className="d-flex align-items-center flex-row mb-3 mb-md-0 mr-md-4">
                  <span className="pr-2">Delivery Location</span>
                  <select onChange={e => {
                    this.setState({
                      deliveryLocationFilter: e.target.value
                    })
                  }}>
                    <option value="All">All</option>
                  </select>
                </div>
                <div className="d-flex align-items-center flex-row mb-3 mb-md-0 mr-md-4">
                  <span className="pr-2">Filter</span>
                  <select onChange={e => {
                    this.setState({
                      typeFilter: e.target.value
                    })
                  }}>
                    <option value="">All</option>
                    <option value="read">Read</option>
                    <option value="unread">Unread</option>
                  </select>
                </div>
                <div className="d-flex align-items-center flex-row">
                  <span className="pr-2">Order Type</span>
                  <select onChange={e => {
                    this.setState({
                      orderTypeFilter: e.target.value
                    })
                  }}>
                    <option value="">All</option>
                    <option value="local">Local</option>
                    <option value="other">Other</option>
                  </select>
                </div>

                <div className="d-flex align-items-center flex-row">
                  <button className="btn btn-flat btn-preview-addcard pull-right border py-1 px-2 ml-2" onSubmit={(e) => this.getOrders(4, e, 'shippingOrders', this.state.orderListCount.shippingOrders)}>
                    GO
                  </button>
                </div>
              </div>
            </form>


            <Table id="customTable" className="table table-striped table-bordered customTable orderTable" responsive>
              <thead>
                <tr>
                  <th>Elapsed Time</th>
                  <th>Order ID</th>
                  <th>Order Date & Time</th>
                  <th>Support Status</th>
                  <th>Shop Category</th>
                  <th>Customer Name</th>
                  <th>Shop Name</th>
                  <th>Distance</th>
                  <th>Product Count</th>
                  <th>Purchase Amount</th>
                  <th>Payment mode</th>
                </tr>
              </thead>
              <tbody>
                {noRecordFound}
                {this.state.orderListData.map((data, index) =>
                  <tr key={index} onClick={() => { this.orderDetailPage(data._id, 4, data.category, data.order_id, 'Shipping', index) }}>
                    <td>
                      <Moment interval={1} duration={data.ordered.at}
                        durationFromNow />
                    </td>
                    <td>{data.order_id}</td>
                    <td><Moment format="D MMM YYYY - h.mm a">{data.ordered.at}</Moment></td>
                    <td>{data.read_receipt_status !== false ? <Button color="primary">Read</Button > : <Button color="danger">Unread</Button >} </td>
                    <td>{data.category}</td>
                    <td>{data.customer.name}</td>
                    <td>{data.store.display_name}</td>
                    <td>{data.distance}</td>
                    <td>{data.product_count}</td>
                    <td>{data.prices.net_price}</td>
                    <td>{data.payment_type}</td>
                  </tr>
                )}
              </tbody>
            </Table>
          </div>
        </TabPane>


        <TabPane tabId="5">
          <div>
            <form ref={(el) => this.orderForm5 = el} onSubmit={(e) => this.getOrders(5, e, 'deliveredOrders', this.state.orderListCount.deliveredOrders)}>
              <div className="col-sm-10 d-flex flex-column flex-md-row justify-content-between mb-2 tableHeader px-0">
                <div className="mr-3 mb-3 mb-md-0">
                  <InputGroup className="d-flex flex-row align-items-center position-relative searchBox">
                    <Input className="pl-4" onKeyUp={e => {
                      this.setState({
                        customerNameFilter: e.target.value
                      })
                    }} placeholder="Customer name, Order Id" />
                  </InputGroup>
                </div>
                <div className="d-flex align-items-center flex-row mb-3 mb-md-0 mr-md-4">
                  <span className="pr-2">Delivery Location</span>
                  <select onChange={e => {
                    this.setState({
                      deliveryLocationFilter: e.target.value
                    })
                  }}>
                    <option value="All">All</option>
                  </select>
                </div>
                <div className="d-flex align-items-center flex-row mb-3 mb-md-0">
                  <span className="pr-2">Filter</span>
                  <select onChange={e => {
                    this.setState({
                      typeFilter: e.target.value
                    })
                  }}>
                    <option value="">All</option>
                    <option value="read">Read</option>
                    <option value="unread">Unread</option>
                  </select>
                </div>
              </div>
              <div className="col-sm-10 d-flex flex-column flex-md-row justify-content-between mb-2 tableHeader px-0 no-gutters">
                <div className="col-md-5 mr-3 mb-3 mb-md-0">
                  <div className="btn-group w-100" role="group" aria-label="Basic example">
                    <button type="button" className={this.state.activeDays === 1 ? 'btn btn-outline-primary btn-block mt-0 text-nowrap active' : 'btn btn-outline-primary btn-block mt-0 text-nowrap'} onClick={
                      () => this.addActiveClass(1)}>Today</button>
                    <button type="button" className={this.state.activeDays === 7 ? 'btn btn-outline-primary btn-block mt-0 text-nowrap active' : 'btn btn-outline-primary btn-block mt-0 text-nowrap'} onClick={
                      () => this.addActiveClass(7)}>Last 7 days</button>
                    <button type="button" className={this.state.activeDays === 30 ? 'btn btn-outline-primary btn-block mt-0 text-nowrap active' : 'btn btn-outline-primary btn-block mt-0 text-nowrap'} onClick={
                      () => this.addActiveClass(30)}>Last 30 days</button>
                  </div>
                </div>
                <div className="d-flex align-items-center flex-row mb-3 mb-md-0 mr-md-4">
                  <label><DatePicker selected={this.state.date1Filter} showYearDropdown closeOnSelect={true} dropdownMode="select"
                    onChange={e => {
                      this.setState({
                        date1Filter: e
                      })
                    }}
                    dateFormat="d MMM yyyy"
                  /> </label>
                </div>
                <div className="d-flex align-items-center flex-row mb-3 mb-md-0 mr-md-4">
                  <label><DatePicker selected={this.state.date2Filter} showYearDropdown closeOnSelect={true} dropdownMode="select"
                    onChange={e => {
                      this.setState({
                        date2Filter: e
                      })
                    }}
                    dateFormat="d MMM yyyy"
                  /></label>
                </div>
                <div className="d-flex align-items-center flex-row">
                  <button className="btn btn-flat btn-preview-addcard pull-right border py-1 px-2 ml-2" onSubmit={(e) => this.getOrders(5, e, 'deliveredOrders', this.state.orderListCount.deliveredOrders)}>
                    GO
                  </button>
                </div>
              </div>
            </form>


            <Table id="customTable" className="table table-striped table-bordered customTable orderTable" responsive>
              <thead>
                <tr>
                  <th>S.no</th>
                  <th>Order ID</th>
                  <th>Order Date & Time</th>
                  <th>Support Status</th>
                  <th>Shop Category</th>
                  <th>Customer Name</th>
                  <th>Shop Name</th>
                  <th>Distance</th>
                  <th>Product Count</th>
                  <th>Purchase Amount</th>
                  <th>Payment mode</th>
                </tr>
              </thead>
              <tbody>
                {noRecordFound}
                {this.state.orderListData.map((data, index) =>
                  <tr key={index} onClick={() => { this.orderDetailPage(data._id, 5, data.category, data.order_id, 'Delivered', index) }}>
                    <td>{index + 1}</td>
                    <td>{data.order_id}</td>
                    <td><Moment format="D MMM YYYY - h.mm a">{data.ordered.at}</Moment></td>
                    <td>{data.read_receipt_status !== false ? <Button color="primary">Read</Button > : <Button color="danger">Unread</Button >} </td>
                    <td>{data.category}</td>
                    <td>{data.customer.name}</td>
                    <td>{data.store.display_name}</td>
                    <td>{data.distance}</td>
                    <td>{data.product_count}</td>
                    <td>{data.prices.net_price}</td>
                    <td>{data.payment_type}</td>
                  </tr>
                )}
              </tbody>
            </Table>
          </div>
        </TabPane>


        <TabPane tabId="6">
          <div>
            <form ref={(el) => this.orderForm6 = el} onSubmit={(e) => this.getOrders(7, e, 'cancelledOrders', this.state.orderListCount.cancelledOrders)}>
              <div className="col-sm-12 d-flex flex-column flex-md-row align-items-center mb-2 tableHeader px-0">
                <div className="mr-3 mb-3 mb-md-0">
                  <InputGroup className="d-flex flex-row align-items-center position-relative searchBox">
                    <Input className="pl-4" onKeyUp={e => {
                      this.setState({
                        customerNameFilter: e.target.value
                      })
                    }} placeholder="Customer name, Order Id" />
                  </InputGroup>
                </div>
                <div className="d-flex align-items-center flex-row mb-3 mb-md-0 mr-md-4">
                  <span className="pr-2">Delivery Location</span>
                  <select onChange={e => {
                    this.setState({
                      deliveryLocationFilter: e.target.value
                    })
                  }}>
                    <option value="All">All</option>
                  </select>
                </div>
                <div className="d-flex align-items-center flex-row mb-3 mb-md-0 mr-md-4">
                  <span className="pr-2">Filter</span>
                  <select onChange={e => {
                    this.setState({
                      typeFilter: e.target.value
                    })
                  }}>
                    <option value="">All</option>
                    <option value="read">Read</option>
                    <option value="unread">Unread</option>
                  </select>
                </div>
                <div className="d-flex align-items-center flex-row">
                  <span className="pr-2">Order Type</span>
                  <select onChange={e => {
                    this.setState({
                      orderTypeFilter: e.target.value
                    })
                  }}>
                    <option value="">All</option>
                    <option value="local">Local</option>
                    <option value="other">Other</option>
                  </select>
                </div>

                <div className="d-flex align-items-center flex-row">
                  <button className="btn btn-flat btn-preview-addcard pull-right border py-1 px-2 ml-2" onSubmit={(e) => this.getOrders(7, e, 'cancelledOrders', this.state.orderListCount.cancelledOrders)}>
                    GO
                  </button>
                </div>
              </div>
            </form>


            <Table id="customTable" className="table table-striped table-bordered customTable orderTable" responsive>
              <thead>
                <tr>
                  <th>S.no</th>
                  <th>Order ID</th>
                  <th>Order Date & Time</th>
                  <th>Support Status</th>
                  <th>Shop Category</th>
                  <th>Customer Name</th>
                  <th>Shop Name</th>
                  <th>Distance</th>
                  <th>Product Count</th>
                  <th>Purchase Amount</th>
                  <th>Payment mode</th>
                </tr>
              </thead>
              <tbody>
                {noRecordFound}
                {this.state.orderListData.map((data, index) =>
                  <tr key={index} onClick={() => { this.orderDetailPage(data._id, 6, data.category, data.order_id, 'Cancelled', index) }}>
                    <td>{index + 1}</td>
                    <td>{data.order_id}</td>
                    <td><Moment format="D MMM YYYY - h.mm a">{data.ordered.at}</Moment></td>
                    <td>{data.read_receipt_status !== false ? <Button color="primary">Read</Button > : <Button color="danger">Unread</Button >} </td>
                    <td>{data.category}</td>
                    <td>{data.customer.name}</td>
                    <td>{data.store.display_name}</td>
                    <td>{data.distance}</td>
                    <td>{data.product_count}</td>
                    <td>{data.prices.net_price}</td>
                    <td>{data.payment_type}</td>
                  </tr>
                )}
              </tbody>
            </Table>
          </div>
        </TabPane>

        <TabPane tabId="7">
          <div>
            <form ref={(el) => this.orderForm7 = el} onSubmit={(e) => this.getOrders(8, e, 'escalatedOrders', this.state.orderListCount.escalatedOrders)}>
              <div className="col-sm-12 d-flex flex-column flex-md-row align-items-center mb-2 tableHeader px-0">

                <div className="mr-3 mb-3 mb-md-0">
                  <InputGroup className="d-flex flex-row align-items-center position-relative searchBox">
                    <Input className="pl-4" onKeyUp={e => {
                      this.setState({
                        customerNameFilter: e.target.value
                      })
                    }} placeholder="Customer name, Order Id" />
                  </InputGroup>
                </div>
                <div className="d-flex align-items-center flex-row mb-3 mb-md-0 mr-md-4">
                  <span className="pr-2">Delivery Location</span>
                  <select onChange={e => {
                    this.setState({
                      deliveryLocationFilter: e.target.value
                    })
                  }}>
                    <option value="All">All</option>
                  </select>
                </div>
                <div className="d-flex align-items-center flex-row mb-3 mb-md-0 mr-md-4">
                  <span className="pr-2">Filter</span>
                  <select onChange={e => {
                    this.setState({
                      typeFilter: e.target.value
                    })
                  }}>
                    <option value="">All</option>
                    <option value="read">Read</option>
                    <option value="unread">Unread</option>
                  </select>
                </div>
                <div className="d-flex align-items-center flex-row">
                  <span className="pr-2">Order Type</span>
                  <select onChange={e => {
                    this.setState({
                      orderTypeFilter: e.target.value
                    })
                  }}>
                    <option value="">All</option>
                    <option value="local">Local</option>
                    <option value="other">Other</option>
                  </select>
                </div>

                <div className="d-flex align-items-center flex-row headerGoButton">
                  <button className="btn btn-flat btn-preview-addcard pull-right border py-1 px-2 ml-2" onSubmit={(e) => this.getOrders(8, e, 'escalatedOrders', this.state.orderListCount.escalatedOrders)}>
                    GO
                  </button>
                </div>
              </div>
            </form>


            <Table id="customTable" className="table table-striped table-bordered customTable orderTable" responsive>
              <thead>
                <tr>
                  <th>Elapsed Time</th>
                  <th>Order ID</th>
                  <th>Order Date & Time</th>
                  <th>Support Status</th>
                  <th>Shop Category</th>
                  <th>Customer Name</th>
                  <th>Shop Name</th>
                  <th>Distance</th>
                  <th>Product Count</th>
                  <th>Purchase Amount</th>
                  <th>Payment mode</th>
                </tr>
              </thead>
              <tbody>
                {noRecordFound}
                {this.state.orderListData.map((data, index) =>
                  <tr key={index} onClick={() => { this.orderDetailPage(data._id, 7, data.category, data.order_id, 'Escalated Order', index) }}>
                    <td>
                      <Moment interval={1} duration={data.ordered.at}
                        durationFromNow />
                    </td>
                    <td>{data.order_id}</td>
                    <td>
                      <Moment format="D MMM YYYY - h.mm a">{data.ordered.at}</Moment>
                    </td>
                    <td>{data.read_receipt_status !== false ? <Button color="primary">Read</Button> : <Button color="danger">Unread</Button>} </td>
                    <td>{data.category}</td>
                    <td>{data.customer.name}</td>
                    <td>{data.store.display_name}</td>
                    <td>{data.distance}</td>
                    <td>{data.product_count}</td>
                    <td>{data.prices.net_price}</td>
                    <td>{data.payment_type}</td>
                  </tr>
                )}
              </tbody>
            </Table>
          </div>

          {/* Order Detail Page */}
        </TabPane>

      </>
    );
  }

  render() {
    let unReadCountNewOrders = (this.state.orderListUnReadCount.newOrders > 0) ?
      <span className="tabsDataCount">{this.state.orderListUnReadCount.newOrders}</span> : '';
    let unReadCountAcceptedOrder = (this.state.orderListUnReadCount.acceptedOrder > 0) ?
      <span className="tabsDataCount">{this.state.orderListUnReadCount.acceptedOrder}</span> : '';

    let unReadCountReadyToShipOrders = (this.state.orderListUnReadCount.readyToShipOrders > 0) ?
      <span className="tabsDataCount">{this.state.orderListUnReadCount.readyToShipOrders}</span> : '';
    let unReadCountShippingOrders = (this.state.orderListUnReadCount.shippingOrders > 0) ?
      <span className="tabsDataCount">{this.state.orderListUnReadCount.shippingOrders}</span> : '';

    let unReadCountDeliveredOrders = (this.state.orderListUnReadCount.deliveredOrders > 0) ?
      <span className="tabsDataCount">{this.state.orderListUnReadCount.deliveredOrders}</span> : '';
    let unReadCountCancelledOrders = (this.state.orderListUnReadCount.cancelledOrders > 0) ?
      <span className="tabsDataCount">{this.state.orderListUnReadCount.cancelledOrders}</span> : '';
    let unReadCountEscalatedOrders = (this.state.orderListUnReadCount.escalatedOrders > 0) ?
      <span className="tabsDataCount">{this.state.orderListUnReadCount.escalatedOrders}</span> : '';

    return (
      <div className="animated fadeIn orderDataPage">
        {this.loader}

        <Row>
          <div className={this.state.customerShopDeliveryContactDetails !== '' ? 'col-md-9' : 'col-md-12'}>
            <Col xs="12" md="12" className="mb-4 px-3 pt-4">
              <Nav className="customTabs mb-3 border-bottom-0" tabs>
                <NavItem>
                  <NavLink
                    active={this.state.activeTab[0] === '1'}
                    onClick={() => { this.toggle(0, '1', 1, 'newOrder', this.state.orderListCount.newOrders); }}
                  >
                    New Order({this.state.orderListCount.newOrders})
                    {unReadCountNewOrders}
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    active={this.state.activeTab[0] === '2'}
                    onClick={() => { this.toggle(0, '2', 2, 'acceptedOrder', this.state.orderListCount.acceptedOrder); }}
                  >
                    Packaging({this.state.orderListCount.acceptedOrder})
                    {unReadCountAcceptedOrder}
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    active={this.state.activeTab[0] === '3'}
                    onClick={() => { this.toggle(0, '3', 3, 'readyToShipOrders', this.state.orderListCount.readyToShipOrders); }}
                  >
                    Dispatched({this.state.orderListCount.readyToShipOrders})
                    {unReadCountReadyToShipOrders}
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    active={this.state.activeTab[0] === '4'}
                    onClick={() => { this.toggle(0, '4', 4, 'shippingOrders', this.state.orderListCount.shippingOrders); }}
                  >
                    Shipping({this.state.orderListCount.shippingOrders})
                    {unReadCountShippingOrders}
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    active={this.state.activeTab[0] === '5'}
                    onClick={() => { this.toggle(0, '5', 5, 'deliveredOrders', this.state.orderListCount.deliveredOrders); }}
                  >
                    Delivered({this.state.orderListCount.deliveredOrders})
                    {unReadCountDeliveredOrders}
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    active={this.state.activeTab[0] === '6'}
                    onClick={() => { this.toggle(0, '6', 7, 'cancelledOrders', this.state.orderListCount.cancelledOrders); }}
                  >
                    Cancelled({this.state.orderListCount.cancelledOrders})
                    {unReadCountCancelledOrders}
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    active={this.state.activeTab[0] === '7'}
                    onClick={() => { this.toggle(0, '7', 8, 'escalatedOrders', this.state.orderListCount.escalatedOrders); }}
                  >
                    Replacement/Undelivered({this.state.orderListCount.escalatedOrders})
                    {unReadCountEscalatedOrders}
                  </NavLink>
                </NavItem>
              </Nav>
              <TabContent className="customTabContent" activeTab={this.state.activeTab[0]}>

                {this.state.orderdetail}
                {this.showTabPane()}
              </TabContent>

            </Col>
          </div>


          {this.state.customerShopDeliveryContactDetails}


        </Row>
      </div>
    );
  }
}

export default withRouter(Orders);
