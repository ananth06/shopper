import React, { Component } from 'react';

class CustomerShopDetails extends Component {
    authToken = '';
    constructor(props) {
        // order value        
        super(props);

        let userData = localStorage.getItem('userData');
        if (userData !== undefined && userData !== null) {
            let authStr = JSON.parse(userData);
            this.authToken = authStr.auth_token;
        }
        this.state = {
            shopContactDetails: this.props.shopContactDetails,
            customerContactDetails: this.props.customerContactDetails,
            deliveryContactDetails: this.props.deliveryContactDetails
        }
    }

    componentDidMount() {

    }

    render() {
        let deliveryLocation = (this.state.deliveryContactDetails.address !== undefined)?  
                        <div className="contentBox mt-3 px-3 py-2 rounded">
                            <p className="mb-2">Delivery Details</p>
                            <p>Address: {this.state.deliveryContactDetails.address.street} {this.state.deliveryContactDetails.address.area} - {this.state.deliveryContactDetails.address.zipcode}</p>
                            <div>
                                <span>Location: </span>
                                <a href={"http://maps.google.co.uk/maps?q="+this.state.deliveryContactDetails.location.coordinates[0]+","+this.state.deliveryContactDetails.location.coordinates[1]} rel='noreferrer noopener' target="_blank">
                                    <img src={'../../assets/img/location.svg'} className="img-avatar" alt="location" width="15" height="15" />
                                </a>
                            </div>
                        </div> : '';
        return (
            <div className="col-md-3 pr-0">
                <div className="twoColRight p-3 h-100">
                    <div className="contentBox mb-3 px-3 py-2 rounded">
                        <p className="mb-2">Shop contact details</p>
                        <p>Name: {this.state.shopContactDetails.name}</p>
                        <p>Mobile number: +{this.state.shopContactDetails.mobile.primary.dialing_code} {this.state.shopContactDetails.mobile.primary.number}</p>
                        {/* <p>Landline: +{this.state.shopContactDetails.mobile.secondary.dialing_code} {this.state.shopContactDetails.mobile.secondary.number}</p> */}
                        <p>Location: {this.state.shopContactDetails.address.location.name}</p>
                    </div>
                    <div className="contentBox mt-3 px-3 py-2 rounded">
                        <p className="mb-2">Customer details</p>
                        <p>Name: {this.state.customerContactDetails.name}</p>
                        <p>Mobile number: +{this.state.customerContactDetails.mobile.dialing_code} {this.state.customerContactDetails.mobile.number}</p>
                    </div>

                    {deliveryLocation}

                    {/* <div className="contentBox mt-3 px-3 py-2 rounded">
                        <p className="mb-2">Support executives</p>
                        <p>Status: Online</p>
                        <p>Name: Ratheesh</p>
                        <p>Login time: 12:12pm</p>
                        <p>Logout time: inprogress</p>
                        <p className="mt-2">Status: Online</p>
                        <p>Name: Shan</p>
                        <p>Login time: 12:12pm</p>
                        <p>Logout time: inprogress</p>
                        <p className="mt-2">Status: Offline</p>
                        <p>Name: John</p>
                        <p>Login time: 12:12pm</p>
                        <p>Logout time: inprogress</p>
                    </div> */}

                </div>
            </div >
        )
    }
}

export default CustomerShopDetails;