import React, { Component } from 'react';
import { Col, Row, Table, Button, InputGroup, Input, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import axios from 'axios';
import Moment from 'react-moment';
import moment from 'moment-timezone';
import Loader from 'react-loader-spinner';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import * as NumericInput from "react-numeric-input";

class OrderDetails extends Component {
    authToken = '';
    loader = '';
    constructor(props) {
        // order value        
        super(props);
        this.props.orderDetails.orderValue = 0;   //add new object in props

        let userData = localStorage.getItem('userData');
        if (userData !== undefined && userData !== null) {
            let authStr = JSON.parse(userData);
            this.authToken = authStr.auth_token;
        }

        this.changeOrderModal = this.changeOrderModal.bind(this);
        this.cancelOrderModal = this.cancelOrderModal.bind(this);
        this.showExpDatesModal = this.showExpDatesModal.bind(this);
        this.productNameSearch = this.productNameSearch.bind(this);
        this.deliveryModal = this.deliveryModal.bind(this);
        this.toggle = this.toggle.bind(this);
        this.orderDetailPageProductSearch = this.orderDetailPageProductSearch.bind(this);
        this.state = {
            changeOrderModal: false,
            activeTab: new Array(4).fill('1'),
            dropdownOpen: false,
            cancelOrderModal: false,
            showExpDatesModal: false,
            orderStatus: [
                '',
                'New Order',
                'Packaging',
                'Dispatched',
                'On The Way',
                'Delivered',
                'Verified',
                'Cancelled By Customer',
                'Rejected By Shop',
                'Cancelled By Super Admin'
            ],
            currentOrderDetail: this.props.currentOrderDetail,
            orderDetails: this.props.orderDetails,
            shopContactDetails: this.props.customerDetails.data.shop,
            customerContactDetails: this.props.customerDetails.data.customer,
            deliveryContactDetails: this.props.customerDetails.data.delivery,
            appliedOfferAmount: 0,
            productSearch: '',
            hasEscalationStatus: this.props.hasEscalationStatus,
            expiryProductIndex: 0,
            totalRefundAmount: 0,
            totalReductionAmount: 0,
            hasReductionPercent: '',
            reductionValue: '',
            reductionTextvalue: '',
            reductionDropdownvalue: '',
            maxReductionValue: 0,
            excludeDeliveryChecked: false,
            hideReductionOption: false,
            displayModalPopupStatus: true,
            deliveryModal: false
        }

        if (this.state.orderDetails !== undefined && this.state.orderDetails !== '') {
            let check = Object.assign({}, this.state.orderDetails);

            // If anytime = true, then custom_time will not be present        
            if (this.state.orderDetails.preferences.custom_time === undefined) {
                if (check.preferences.custom_time === undefined) {
                    check.preferences.custom_time = {};
                    check.preferences.custom_time.to = '';
                }
            } else {
                // set Timezone UTC -5.30
                var selectedDate = '';
                selectedDate = moment(this.state.orderDetails.preferences.custom_time.to, "YYYY-MM-DD[T]HH:mm:ss.SSSZZ");
                check.preferences.custom_time.to = selectedDate.subtract(0.0, 'hour');
            }

            if (this.state.orderDetails.preferences.expected_time === undefined) {
                check.preferences.expected_time = {};
                check.preferences.expected_time.to = '';
                check.preferences.expected_time.at = '';
            } else {
                // set Timezone UTC -5.30
                var selectedDate1 = '';
                selectedDate1 = moment(this.state.orderDetails.preferences.expected_time.at, "YYYY-MM-DD[T]HH:mm:ss.SSSZZ");
                check.preferences.expected_time.at = selectedDate1.subtract(0.0, 'hour');
            }

            // display CV Status if status is Delivery Tab
            if (this.state.orderDetails.status === 6) {
                for (let i = 0; i < this.state.orderDetails.products.length; i++) {
                    if (this.state.orderDetails.products[i].cv_status === 1) {
                        check.products[i].cv_status = 'Delivered';
                    }
                    if (this.state.orderDetails.products[i].cv_status === 2) {
                        check.products[i].cv_status = 'Undelivered';
                    }
                    if (this.state.orderDetails.products[i].cv_status === 3) {
                        check.products[i].cv_status = 'Replacement';
                    }
                }
            }

            this.setState({
                orderDetails: check
            });
        }
        this.returnToOrderListPage = this.returnToOrderListPage.bind(this)
    }

    orderDetailPageProductSearch(event, status) {
        if (event !== '' && event !== null && event !== undefined) {
            event.preventDefault();
        }
        let productSearch = '?text=' + this.state.productSearch;
        if (status) {
            productSearch = '';
        }
        let orderId = (this.state.orderDetails._id !== undefined) ? this.state.orderDetails._id : this.props.orderDetails._id;
        this.loader = <div className="cutomLoader"> <Loader type="Oval" color="#00BFFF" height="100" width="100" /> </div>;
        axios.get(process.env.REACT_APP_API_BASE_URL + 'order/' + orderId + '/info' + productSearch, { 'headers': { 'Authorization': 'JWT ' + this.authToken } }).then((response) => {
            this.loader = '';
            if (response.data.data._id !== undefined) {
                this.setState({
                    orderDetails: response.data.data,
                    displayModalPopupStatus: true
                });


                let check = Object.assign({}, this.state.orderDetails);
                // If anytime = true, then orderValue will not be present        
                if (check.orderValue === undefined) {
                    check.orderValue = check.amount_paid_by_customer;
                }
                this.setState({
                    orderDetails: check
                });
                this.componentDidMount();
            } else {
                this.setState({
                    displayModalPopupStatus: false
                });
            }
        }, error => {
            return error;
        });
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     return true;
    // }

    componentDidMount() {
        let order_value = Object.assign({}, this.state.orderDetails);
        if (this.props.orderDetails.prices.delivery !== undefined) {
            if (order_value.prices.delivery !== undefined) {
                order_value.orderValue = order_value.prices.delivery + order_value.prices.net_price;
            } else {
                order_value.orderValue = order_value.prices.net_price;
            }
            order_value.orderValue = order_value.orderValue.toFixed(2);
        } else {
            order_value.orderValue = order_value.prices.net_price.toFixed(2);
            order_value.prices.delivery = 0;
        }

        let orderValue = order_value.orderValue;
        if (orderValue.toString().indexOf('.') !== -1) {
            let orderSplitValue = order_value.orderValue.toString().split('.');
            orderValue = orderSplitValue[0];
        }

        this.setState({
            maxReductionValue: orderValue,
            totalRefundAmount: order_value.orderValue
        });

        // for General Purpose to display float value with precision(2)
        order_value.upc_commission_cost = order_value.upc_commission_cost.toFixed(2);
        order_value.merchantAmount = order_value.merchantAmount.toFixed(2);
        order_value.amount_paid_by_customer = order_value.amount_paid_by_customer.toFixed(2);
        order_value.sku_commission_cost = order_value.sku_commission_cost.toFixed(2);

        // payment_fee will come under prices only if it is a online payment order.
        if (this.props.orderDetails.prices.payment_fee === undefined) {
            order_value.prices.payment_fee = 0;
        }

        this.setState({
            orderDetails: order_value
        });

        /** Show and Hide Reduction Option in Cancel Order Popup Based on Payment Type */
        let paymentType = this.state.orderDetails.payment_type.name;
        if (paymentType.toLowerCase() === "cash") {
            this.setState({ hideReductionOption: true })
        }
    }

    changeOrderModal() {
        this.setState({
            changeOrderModal: !this.state.changeOrderModal,
        });
    }

    cancelOrderModal() {
        this.setState({
            cancelOrderModal: !this.state.cancelOrderModal
        });
    }

    showExpDatesModal(i) {
        this.setState({
            showExpDatesModal: !this.state.showExpDatesModal,
            expiryProductIndex: i
        });
    }

    toggle() {
        this.setState(prevState => ({
            dropdownOpen: !prevState.dropdownOpen
        }));
    }

    productNameSearch(productSearch) {
        if (productSearch === '') {
            this.setState({
                productSearch: ''
            });
            this.orderDetailPageProductSearch('', true);
        }
    }

    returnToOrderListPage() {
        localStorage.setItem('backButtonStatus', true);
        this.props.backButton(false)
    }

    cancelOrder(event) {
        event.preventDefault();
        if (this.authToken !== '') {
            this.loader = <div className="cutomLoader"> <Loader type="Oval" color="#00BFFF" height="100" width="100" /> </div>;
            let postData = {
                "order_id": this.props.orderId,
                "shop_id": this.props.orderDetails.store_id,
                // "has_reduction_percent": this.state.hasReductionPercent,
                // "reduction_value": this.state.reductionValue,
                "has_excluded_delivery": this.state.excludeDeliveryChecked
            }

            if (this.state.hasReductionPercent !== '') {
                postData.has_reduction_percent = this.state.hasReductionPercent
            }
            if (this.state.reductionValue !== '') {
                postData.reduction_value = this.state.reductionValue
            }

            axios.post(process.env.REACT_APP_API_BASE_URL + 'order/cancelOrder', postData, { 'headers': { 'Authorization': 'JWT ' + this.authToken } }).then((response) => {
                this.loader = '';
                if (response.data.success) {
                    this.setState({
                        cancelOrderModal: !this.state.cancelOrderModal
                    });
                    toast.success(response.data.message);
                }
            }, error => {
                this.loader = '';
                toast.error(error.response.data.message);
                return error;
            });

        }
    }

    /* deliveryModal Confirmation Popup */
    deliveryModal() {
        this.setState({
            deliveryModal: !this.state.deliveryModal,
        });
    }

    deliveryOrder() {
        if (this.authToken !== '') {
            console.log(this.props);
            this.loader = <div className="cutomLoader"> <Loader type="Oval" color="#00BFFF" height="100" width="100" /> </div>;
            let postData = {
                "order_id": this.props.orderDetails._id,
                "shop_id": this.props.orderDetails.store_id,
                "category_id": this.props.orderDetails.category_id
            }    
    
            axios.post(process.env.REACT_APP_API_BASE_URL + 'order/deliverOrder', postData, { 'headers': { 'Authorization': 'JWT ' + this.authToken } }).then((response) => {
                this.loader = '';
                if (response.data.success) {
                    this.setState({
                        deliveryModal: !this.state.deliveryModal
                    });
                    toast.success(response.data.message);
                    this.props.orderListPage();
                }
            }, error => {
                this.loader = '';
                toast.error(error.response.data.message);
                return error;
            });

        }
    }

    render() {
        let payment_method_header = null;
        let payment_method_content = [];
        let mfg_header = null;
        let mfg_content = [];
        let cv_status_header = null;
        let cv_status_content = [];
        let cutomer_feedback_button = '';
        let deliveryboy_button = '';
        let expect_delivery_time_button = <div>Expected Delivery time:</div>;
        let sku_value = [];
        let cancel_order_button = '';
        let delivery_order_button = '';
        let customerDateTime = '';
        let expectedDateTime = '';
        let row_content = '';
        let delivery_mode = '';
        let order_status = '';
        let payment_fee_percent = '';
        let expiryProductName = [];
        let tablebody_content = <tbody><tr><td colSpan="5">No Record Found</td></tr></tbody>;
        let exptablebody_content = [];
        let display_manufacture_date = [];
        let display_months_left = [];
        const hideReductionOption = this.state.hideReductionOption;


        if (this.state.orderDetails !== undefined && this.state.orderDetails.customer_id !== undefined && this.state.orderDetails.customer_id !== '' && this.state.displayModalPopupStatus) {
            /** Display Card payment fee */
            payment_fee_percent = (this.state.orderDetails.payment_fee_percent !== undefined) ? this.state.orderDetails.payment_fee_percent : 0;

            /** Display Delivery mode and Order Status */
            delivery_mode = this.state.orderDetails.delivery_type.name;
            order_status = this.state.orderStatus[this.state.orderDetails.status];
            if (this.state.orderDetails.status === 6 && this.state.hasEscalationStatus) {
                order_status = 'Escalated';     // this is for Escalated Status Tab
            }

            /* Display Cancel Order Button for first Four Tabs */
            if (this.state.orderDetails.status <= 4) {
                cancel_order_button = <Button onClick={this.cancelOrderModal} className="mr-3">Cancel Order</Button>
            }

            /* Display Delivery Order Button for Dispatched & Shipping Tabs */
            if (this.state.orderDetails.status === 4 || this.state.orderDetails.status === 3) {
                delivery_order_button = <Button onClick={this.deliveryModal} className="mr-3">Deliver Order</Button>
            }

            /* Display UPC/SKU value */
            for (let i = 0; i < this.state.orderDetails.products.length; i++) {
                if (this.state.orderDetails.products[i].upc !== undefined) {
                    sku_value[i] = <td>{this.state.orderDetails.products[i].upc}</td>
                } else {
                    sku_value[i] = <td>{this.state.orderDetails.products[i].sku}</td>
                }
            }

            /* Display CV Status in Delivered and Cancelled Tab */
            if ((this.state.orderDetails.status === 5) || (this.state.orderDetails.status === 6) || this.state.orderDetails.status === 7 || this.state.orderDetails.status === 8 || this.state.orderDetails.status === 9) {
                cv_status_header = <th>CV Status</th>;
                for (let i = 0; i < this.state.orderDetails.products.length; i++) {
                    cv_status_content[i] = <td>Cancelled</td>
                    if (this.state.orderDetails.status === 6 || this.state.orderDetails.status === 5) {
                        if (this.state.orderDetails.products[i].cv_status !== undefined &&
                            this.state.orderDetails.products[i].cv_status !== null) {
                            cv_status_content[i] = <td>{this.state.orderDetails.products[i].cv_status}</td>;
                        } else {
                            cv_status_content[i] = <td>-</td>;
                        }
                    }
                }
            }

            /* Display Ordered Delivered Button in Delivered Tab */
            if (this.state.orderDetails.status === 5 || this.state.orderDetails.status === 6) {
                expect_delivery_time_button = <div>Order Delivered on:</div>;
            }

            /* Display Expected Delivery Time button, Order Cancelled on button Based on Stage */
            /* For Customer Feedback Button in Cancelled Tab */
            if (this.state.orderDetails.status === 7 || this.state.orderDetails.status === 8 || this.state.orderDetails.status === 9) {
                expect_delivery_time_button = <div>Order Cancelled on:</div>;
                cutomer_feedback_button = <Button className="text-primary">Customer Feedback</Button>
            }

            /* For Assign Delivery Boy Button in Packaging, Dispatched Status Tab */
            if (this.state.orderDetails.status === 1 || this.state.orderDetails.status === 2 || this.state.orderDetails.status === 3) {
                // deliveryboy_button = <Button className="text-primary">Assign to delivery boy</Button>;
            }

            /* To Display Packaging Type, MFG & Exp Date */
            if (this.state.orderDetails.status !== 1 && this.state.orderDetails.status !== 2 && !this.state.hasEscalationStatus) {
                payment_method_header = <th>Package Method</th>;
                mfg_header = <th>MFG & EXP</th>;
                for (let i = 0; i < this.state.orderDetails.products.length; i++) {
                    /* For Packaging Type */
                    if (this.state.orderDetails !== undefined && this.state.orderDetails.products[i].packaging_type !== undefined) {
                        payment_method_content[i] = <td>{this.state.orderDetails.products[i].packaging_type}</td>;
                    } else {
                        payment_method_content[i] = <td> - </td>;
                    }

                    /* For MFG & Exp Date */
                    if (this.state.orderDetails !== undefined && this.state.orderDetails.products[i].mfd_exp_dates !== undefined) {
                        if (this.state.orderDetails.products[i].mfd_exp_dates.type === 1) {
                            display_manufacture_date[i] = <td>-</td>;
                            display_months_left[i] = <td>-</td>;
                            if (this.state.orderDetails.products[i].mfd_exp_dates.dates[0].exp_date !== undefined) {
                                if (this.state.orderDetails.products[i].mfd_exp_dates.dates[0].mfg_date !== undefined) {
                                    display_manufacture_date[i] = <td><Moment format="DD/MM/YYYY">{this.state.orderDetails.products[i].mfd_exp_dates.dates[0].mfg_date}</Moment></td>;
                                }

                                // display_months_left[i] = <td><Moment duration={this.state.orderDetails.products[i].mfd_exp_dates.dates[0].mfg_date} date={this.state.orderDetails.products[i].mfd_exp_dates.dates[0].exp_date} /></td>;

                                display_months_left[i] = <td><Moment duration={this.state.currentOrderDetail.accepted.at} date={this.state.orderDetails.products[i].mfd_exp_dates.dates[0].exp_date} /></td>;
                            }
                            mfg_content[i] = <td><Button onClick={(e) => {
                                this.showExpDatesModal(i);
                            }} className="mx-auto d-flex flex-row align-items-center viewButton"> <img src={'../../assets/img/viewicon.png'} width="15" alt="" /><span className="ml-1">View</span></Button></td>;
                            expiryProductName[i] = this.state.orderDetails.products[i].product_name;
                            exptablebody_content[i] = <tbody>
                                <tr>
                                    {display_manufacture_date[i]}
                                    <td><Moment format="DD/MM/YYYY">{this.state.orderDetails.products[i].mfd_exp_dates.dates[0].exp_date}</Moment></td>
                                    {display_months_left[i]}
                                </tr>
                            </tbody>
                            // mfg_content[i] = <td>MFG Date: <Moment format="DD/MM/YYYY">{this.state.orderDetails.products[i].mfd_exp_dates.dates[0].mfg_date}</Moment><br /> Exp Date: <Moment format="DD/MM/YYYY">{this.state.orderDetails.products[i].mfd_exp_dates.dates[0].exp_date}</Moment></td>;
                        } else if (this.state.orderDetails.products[i].mfd_exp_dates.type === 2) {
                            mfg_content[i] = <td>Not Applicable</td>;
                        } else {
                            mfg_content[i] = <td>Unknown</td>;
                        }
                    } else {
                        mfg_content[i] = <td>-</td>;
                    }
                }
            }

            customerDateTime = (this.state.orderDetails !== undefined && this.state.orderDetails.preferences !== undefined && this.state.orderDetails.preferences.custom_time !== undefined && this.state.orderDetails.preferences.custom_time.to !== '') ? <Moment format="DD/MM/YYYY - hh:mm A">{this.state.orderDetails.preferences.custom_time.to}</Moment> : 'Immediate';
            // alert(this.state.orderDetails.status + " && "+this.state.orderDetails.preferences.expected_time.at);

            /* To Display Expiry Date and Time, Cancelled order Time based on Status (New order, Cancelled etc.,) */
            if (this.state.orderDetails.status !== 7 && this.state.orderDetails.status !== 8 && this.state.orderDetails.status !== 9 && this.state.orderDetails.status !== 5 && this.state.orderDetails.status !== 6) {
                let selectedDate = '';
                if (this.state.orderDetails !== undefined && this.state.orderDetails.preferences !== undefined && this.state.orderDetails.preferences.expected_time !== undefined && this.state.orderDetails.preferences.expected_time.at !== '') {
                    selectedDate = moment(this.state.orderDetails.preferences.expected_time.at, "YYYY-MM-DD[T]HH:mm:ss.SSSZZ");
                    expectedDateTime = selectedDate.subtract(5.5, 'hour');
                    expectedDateTime = <Moment format="DD/MM/YYYY - hh:mm A">{expectedDateTime}</Moment>;
                }
            }
            else if (this.state.orderDetails.status === 5 || this.state.orderDetails.status === 6) {
                let selectedDate1 = '';
                if (this.state.orderDetails !== undefined && this.state.orderDetails.delivered !== undefined && this.state.orderDetails.delivered.at !== undefined && this.state.orderDetails.delivered.at !== '') {
                    selectedDate1 = moment(this.state.orderDetails.delivered.at, "YYYY-MM-DD[T]HH:mm:ss.SSSZZ");
                    // expectedDateTime = selectedDate1.subtract(5.5, 'hour');
                    expectedDateTime = <Moment format="DD/MM/YYYY - hh:mm A">{selectedDate1}</Moment>;
                }
            }
            else {
                let selectedDate2 = '';
                //alert(this.state.orderDetails.status);
                if (this.state.orderDetails.status === 7) {
                    if (this.state.orderDetails.cancelled !== undefined && this.state.orderDetails.cancelled.at !== undefined) {
                        selectedDate2 = moment(this.state.orderDetails.cancelled.at, "YYYY-MM-DD[T]HH:mm:ss.SSSZZ");
                        // expectedDateTime = selectedDate2.subtract(5.5, 'hour');
                        expectedDateTime = <Moment format="DD/MM/YYYY - hh:mm A">{selectedDate2}</Moment>;
                    }
                } else if (this.state.orderDetails.status === 8) {
                    if (this.state.orderDetails.rejected !== undefined && this.state.orderDetails.rejected.at !== undefined) {
                        selectedDate2 = moment(this.state.orderDetails.rejected.at, "YYYY-MM-DD[T]HH:mm:ss.SSSZZ");
                        // expectedDateTime = selectedDate2.subtract(5.5, 'hour');
                        expectedDateTime = <Moment format="DD/MM/YYYY - hh:mm A">{selectedDate2}</Moment>;
                    }
                } else {
                    if (this.state.orderDetails.denied !== undefined && this.state.orderDetails.denied.at !== undefined) {
                        selectedDate2 = moment(this.state.orderDetails.denied.at, "YYYY-MM-DD[T]HH:mm:ss.SSSZZ");
                        // expectedDateTime = selectedDate2.subtract(5.5, 'hour');
                        expectedDateTime = <Moment format="DD/MM/YYYY - hh:mm A">{selectedDate2}</Moment>;
                    }
                }
            }


            /** Display Table Dynamic Product Content */
            tablebody_content = <tbody>
                {this.state.orderDetails.products.map((data, index) =>
                    <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{data.product_name}</td>
                        {sku_value[index]}
                        <td>{data.qty}</td>
                        {payment_method_content[index]}
                        <td>{data.amount}</td>
                        {mfg_content[index]}
                        {cv_status_content[index]}
                    </tr>
                )}
            </tbody>

            /** Display Row Content (Applied Offer amount, Card payment fee, etc.,) after Table */
            row_content = <Row className="orderDetailsData pb-4">
                <Col className="font-weight-bold oddcol-01 px-0 threeColDetails" xs="12" md="4">
                    <div>
                        <label className="mb-0 text-right mr-4">Applied Offer amount:</label><span>{this.state.appliedOfferAmount}</span>
                    </div>
                    <div>
                        <label className="mb-0 text-right mr-4">Total products:</label><span>{this.state.orderDetails.total_products}</span>
                    </div>
                    <div>
                        <label className="mb-0 text-right mr-4 float-left">Amount paid by customer:</label><span className="float-left breakWord">{this.state.orderDetails.amount_paid_by_customer}</span>
                    </div>
                    <div>
                        <label className="mb-0 text-right mr-4">Tax if any:</label><span>{this.state.orderDetails.prices.taxes}</span>
                    </div>
                </Col>
                <Col className="font-weight-bold oddcol-02 threeColDetails" xs="12" md="4">
                    <div>
                        <label className="mb-0 text-right mr-4">Card payment fee %:</label><span>{this.state.orderDetails.prices.payment_fee} ({payment_fee_percent}%)</span>
                    </div>
                    <div>
                        <label className="mb-0 text-right mr-4">UPC commission cost %:</label><span>{this.state.orderDetails.upc_commission_cost} ({this.state.orderDetails.upc_commission_percentage}%)</span>
                    </div>
                    <div>
                        <label className="mb-0 text-right mr-4">SKU commission cost %:</label><span>{this.state.orderDetails.sku_commission_cost} ({this.state.orderDetails.sku_commission_percentage}%)</span>
                    </div>
                    <div>
                        <label className="mb-0 text-right mr-4">Amount to be paid to merchant:</label><span>{this.state.orderDetails.merchantAmount}</span>
                    </div>
                </Col>
                <Col className="font-weight-bold oddcol-03 threeColDetails" xs="12" md="4">
                    <div>
                        <label className="mb-0 text-right mr-2">Delivery Charge:</label><span>{this.state.orderDetails.prices.delivery}</span>
                    </div>
                    <div>
                        <label className="mb-0 text-right mr-2">Delivery Mode:</label><span>{this.state.orderDetails.delivery_type.name}</span>
                    </div>
                    <div>
                        <label className="mb-0 text-right mr-2">Payment Mode:</label><span>{this.state.orderDetails.payment_type.name}</span>
                    </div>
                </Col>
                <Col className="font-weight-bold" xs="12" md="12">
                    <Row className="align-items-center">
                        <Col xs="12" md="9">
                            <Row className="no-gutters">
                                <Col md="8" className="text-right">
                                    <div>Customer Preferred Delivery time & date: </div>
                                </Col>
                                <Col md="4">
                                    <span className="text-primary ml-3">
                                        {customerDateTime}
                                    </span>
                                </Col>
                            </Row>
                        </Col>
                        <Col xs="12" className="text-sm-right text-center px-0" md="3">
                            {deliveryboy_button}
                            {cutomer_feedback_button}
                        </Col>

                    </Row>
                    <Row className="align-items-center">
                        <Col xs="12" md="9">
                            <Row className="no-gutters">
                                <Col md="8" className="text-right">
                                    {expect_delivery_time_button}
                                </Col>
                                <Col md="4">
                                    <span className="text-primary ml-3">{expectedDateTime}</span>
                                </Col>
                            </Row>
                        </Col>
                        <Col xs="12" className="" md="3">Order Value: {this.state.orderDetails.orderValue}</Col>
                    </Row>

                </Col>
                {/* <Col xs="12" md="12" className="py-3 px-0">

                        <div className="col-sm-12 px-0 d-flex flex-row orderValueSec">
                            <div className="col-md-3 px-0">
                                <h5 className="font-weight-bold">Support Log</h5>
                            </div>
                            <div className="col-md-5 px-0">
                                <div>Expected Delivery time: <span className="text-primary ml-3">{expectedDateTime}</span></div>
                            </div>
                            <div className="col-md-4 px-0 text-center">
                                <h5 className="font-weight-bold">Order Value: {this.state.orderDetails.orderValue}</h5>
                            </div>
                        </div>
                        <div className="border supportLog p-2">
                            <div className="logTitle">
                                <span className="pl-0 pr-2 font-weight-bold">Ratheesh:</span><span className="pl-0">20/03/2019 - 10:15am</span>
                            </div>
                            <div className="logContent">
                                <p>Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.</p>
                            </div>
                        </div>
                    </Col> */}
                <Col xs="12" md="12" className="py-3 orderButtonsList">
                    <Row className="align-items-center">
                        <Col xs="12" className="text-center px-0" lg="4">
                            {/* <Button className="mr-3">View log</Button> */}
                        </Col>
                        <Col xs="12" className="text-lg-right text-center mt-3 px-0" lg="8">
                            {cancel_order_button}
                            {delivery_order_button}
                            <Button className="backButton" color="primary" onClick={(e) => {
                                this.returnToOrderListPage();
                            }}>Back</Button>
                        </Col>
                    </Row>
                </Col>
            </Row>
        }

        return (
            <div>
                {this.loader}
                <div>
                    <ul className="d-flex list-unstyled">
                        <li>{this.props.orderCategory}</li>
                        <li className="px-2">></li>
                        <li>{this.props.tabName}</li>
                        <li className="px-2">></li>
                        <li className="lightGrey">Order Id: {this.props.orderId}</li>
                    </ul>
                </div>
                <div className="orderDetailPage">
                    <div>
                        <h3 className="text-center pt-3">Order Details</h3>
                        <div className="col-sm-12 d-flex flex-md-row flex-column align-items-center py-2 px-0 tableHeader">
                            <div className="col-sm-4 mr-4 d-flex mb-md-0 mb-3 flex-row flex-wrap align-items-center pl-0">
                                <InputGroup className="position-relative searchBox w-75">
                                    <Input className="pl-4 w-100" placeholder="Search product" onKeyUp={e => {
                                        this.setState({
                                            productSearch: e.target.value
                                        });
                                        this.productNameSearch(e.target.value);
                                    }} />
                                </InputGroup>
                                <span className="w-25"><i className="fa fa-search ml-2" aria-hidden="true" onClick={(e) => this.orderDetailPageProductSearch(e, false)}></i></span>
                            </div>
                            <div className="col-sm-4 d-flex flex-row mb-md-0 mb-3 flex-wrap align-items-center mr-4">
                                <label className="font-weight-bold mb-0 pr-3">Delivery Mode:</label>
                                <span className="text-success font-weight-bold">{delivery_mode}</span>
                            </div>
                            <div className="col-sm-4 d-flex flex-row flex-wrap align-items-center mr-4">
                                <label className="font-weight-bold mb-0 pr-3">Order Status:</label>
                                <span className="text-primary font-weight-bold">{order_status}</span>
                            </div>
                        </div>
                    </div>

                    <div className="px-0">
                        <Table id="customTable" className="table table-striped table-bordered customTable" responsive>
                            <thead>
                                <tr>
                                    <th>SNo</th>
                                    <th>Product Name</th>
                                    <th>UPC/SKU</th>
                                    <th>Quantity</th>
                                    {payment_method_header}
                                    <th>Amount</th>
                                    {mfg_header}
                                    {cv_status_header}
                                </tr>
                            </thead>
                            {tablebody_content}
                        </Table>
                    </div>
                </div>

                {row_content}

                <Modal isOpen={this.state.cancelOrderModal} className="customPopUp cancelOrders">
                    <form onSubmit={(e) => this.cancelOrder(e)}>
                        <ModalHeader className="position-relative" toggle={this.toggle}>Cancel Order
                        <Button className="closeButton" onClick={this.cancelOrderModal}>&times;</Button>{' '}
                        </ModalHeader>
                        <ModalBody className="p-0">
                            <Row className="align-items-center">
                                <Col xs="12" className="text-center px-0" md="12">
                                    <Row>
                                        <Col md="5" className="text-right">Order Value:</Col>
                                        <Col md="7" className="text-left">{this.state.orderDetails.orderValue}</Col>
                                    </Row>

                                    {!hideReductionOption && (
                                        <div>
                                            <Row>
                                                <Col md="5" className="text-right">Reduction (%):</Col>
                                                <Col md="7" className="text-left">
                                                    <select value={this.state.reductionDropdownvalue} onChange={
                                                        (e) => {
                                                            this.setState({
                                                                hasReductionPercent: false,
                                                                reductionValue: e.target.value,
                                                                reductionDropdownvalue: e.target.value,
                                                                totalRefundAmount: this.state.orderDetails.orderValue
                                                            })
                                                            if (e.target.value > 0) {
                                                                let total_reduction_amount = 0;
                                                                total_reduction_amount = (Number(e.target.value) / 100) * Number(this.state.orderDetails.orderValue);
                                                                if (this.state.excludeDeliveryChecked) {
                                                                    total_reduction_amount = Number(total_reduction_amount) + Number(this.state.orderDetails.prices.delivery);
                                                                }

                                                                if ((Number(this.state.orderDetails.orderValue) - Number(total_reduction_amount)).toFixed(2) > 0) {
                                                                    this.setState({
                                                                        hasReductionPercent: true,
                                                                        reductionTextvalue: '',
                                                                        totalReductionAmount: total_reduction_amount.toFixed(2),
                                                                        totalRefundAmount: (Number(this.state.orderDetails.orderValue) - Number(total_reduction_amount)).toFixed(2)
                                                                    })
                                                                } else {
                                                                    this.setState({
                                                                        reductionDropdownvalue: 0,
                                                                        totalReductionAmount: 0,
                                                                        reductionValue: 0,
                                                                        reductionTextvalue: '',
                                                                        totalRefundAmount: this.state.orderDetails.orderValue,
                                                                        excludeDeliveryChecked: false
                                                                    })
                                                                    toast.error("Total Refund Amount is not below 0");
                                                                }
                                                            } else {
                                                                if (this.state.excludeDeliveryChecked) {
                                                                    let total_refund_amount = parseFloat(Number(this.state.orderDetails.orderValue) - Number(this.state.orderDetails.prices.delivery)).toFixed(2);

                                                                    let total_reduction_amount = parseFloat(Number(this.state.orderDetails.prices.delivery)).toFixed(2);
                                                                    if (total_refund_amount > 0) {
                                                                        this.setState({
                                                                            reductionDropdownvalue: 0,
                                                                            totalReductionAmount: total_reduction_amount,
                                                                            totalRefundAmount: total_refund_amount
                                                                        })
                                                                    } else {
                                                                        this.setState({
                                                                            reductionDropdownvalue: 0,
                                                                            totalReductionAmount: 0,
                                                                            reductionValue: 0,
                                                                            reductionTextvalue: '',
                                                                            totalRefundAmount: this.state.orderDetails.orderValue,
                                                                            excludeDeliveryChecked: false
                                                                        })
                                                                        toast.error("Total Refund Amount is not below 0");
                                                                    }
                                                                } else {
                                                                    this.setState({
                                                                        reductionDropdownvalue: 0,
                                                                        totalReductionAmount: 0,
                                                                        totalRefundAmount: this.state.orderDetails.orderValue
                                                                    })
                                                                }
                                                            }
                                                        }
                                                    }>
                                                        <option value="0">0%</option>
                                                        <option value="5">5%</option>
                                                        <option value="10">10%</option>
                                                        <option value="20">20%</option>
                                                        <option value="25">25%</option>
                                                        <option value="50">50%</option>
                                                    </select>
                                                    <span> OR </span>
                                                    <NumericInput min="0" max={this.state.maxReductionValue} onKeyUp={
                                                        (e) => {
                                                            this.setState({
                                                                hasReductionPercent: false,
                                                                reductionValue: e.target.value,
                                                                reductionTextvalue: e.target.value
                                                            })
                                                            if (e.target.value > 0) {
                                                                let total_reduction_amount = 0;
                                                                total_reduction_amount = Number(this.state.orderDetails.orderValue) - Number(e.target.value);

                                                                if (this.state.excludeDeliveryChecked) {
                                                                    total_reduction_amount = Number(total_reduction_amount) - Number(this.state.orderDetails.prices.delivery);
                                                                }
                                                                if (total_reduction_amount >= 0) {
                                                                    this.setState({
                                                                        reductionDropdownvalue: 0,
                                                                        totalReductionAmount: (Number(this.state.orderDetails.orderValue) - Number(total_reduction_amount)).toFixed(2),
                                                                        totalRefundAmount: total_reduction_amount.toFixed(2)
                                                                    })
                                                                } else {
                                                                    this.setState({
                                                                        reductionDropdownvalue: 0,
                                                                        totalReductionAmount: 0,
                                                                        reductionTextvalue: '',
                                                                        reductionValue: 0,
                                                                        totalRefundAmount: this.state.orderDetails.orderValue,
                                                                        excludeDeliveryChecked: false
                                                                    })
                                                                    toast.error("Total Refund Amount is not below 0");
                                                                }
                                                            } else {
                                                                if (this.state.excludeDeliveryChecked) {
                                                                    let total_refund_amount = parseFloat(Number(this.state.orderDetails.orderValue) - Number(this.state.orderDetails.prices.delivery)).toFixed(2);

                                                                    let total_reduction_amount = parseFloat(Number(this.state.orderDetails.prices.delivery)).toFixed(2);
                                                                    if (total_refund_amount > 0) {
                                                                        this.setState({
                                                                            reductionDropdownvalue: 0,
                                                                            totalReductionAmount: total_reduction_amount,
                                                                            totalRefundAmount: total_refund_amount
                                                                        })
                                                                    } else {
                                                                        this.setState({
                                                                            reductionDropdownvalue: 0,
                                                                            totalReductionAmount: 0,
                                                                            reductionTextvalue: '',
                                                                            reductionValue: 0,
                                                                            totalRefundAmount: this.state.orderDetails.orderValue,
                                                                            excludeDeliveryChecked: false
                                                                        })
                                                                        toast.error("Total Refund Amount is not below 0");
                                                                    }
                                                                } else {
                                                                    this.setState({
                                                                        reductionDropdownvalue: 0,
                                                                        totalReductionAmount: 0,
                                                                        reductionTextvalue: '',
                                                                        reductionValue: 0,
                                                                        totalRefundAmount: this.state.orderDetails.orderValue
                                                                    })
                                                                }
                                                            }
                                                        }
                                                    } value={this.state.reductionTextvalue} placeholder="Value" />
                                                </Col>
                                            </Row>


                                            <Row>
                                                <Col md="5" className="text-right">Excluded Delivery Charge:</Col>
                                                <Col md="7" className="text-left d-flex flex-row align-items-center"><span className="mr-2">{this.state.orderDetails.prices.delivery}</span> <input checked={this.state.excludeDeliveryChecked} type="checkbox"
                                                    onChange={e => {
                                                        this.setState({
                                                            excludeDeliveryChecked: !this.state.excludeDeliveryChecked,
                                                        });
                                                        setTimeout(() => {
                                                            if (this.state.excludeDeliveryChecked) {
                                                                let total_refund_amount = parseFloat(Number(this.state.orderDetails.orderValue) - (Number(this.state.totalReductionAmount) + Number(this.state.orderDetails.prices.delivery))).toFixed(2);

                                                                let total_reduction_amount = parseFloat((Number(this.state.totalReductionAmount) + Number(this.state.orderDetails.prices.delivery))).toFixed(2);
                                                                if (total_refund_amount > 0) {
                                                                    this.setState({
                                                                        totalReductionAmount: total_reduction_amount,
                                                                        totalRefundAmount: total_refund_amount
                                                                    })
                                                                } else {
                                                                    this.setState({
                                                                        reductionDropdownvalue: 0,
                                                                        totalReductionAmount: 0,
                                                                        reductionTextvalue: '',
                                                                        reductionValue: 0,
                                                                        totalRefundAmount: this.state.orderDetails.orderValue,
                                                                        excludeDeliveryChecked: false
                                                                    })
                                                                    toast.error("Total Refund Amount is not below 0");
                                                                }
                                                            } else {
                                                                let refundAmount = this.state.totalReductionAmount;
                                                                if (Number(this.state.totalReductionAmount) >= Number(this.state.orderDetails.prices.delivery)) {
                                                                    refundAmount = parseFloat(Number(this.state.totalReductionAmount) - Number(this.state.orderDetails.prices.delivery)).toFixed(2);
                                                                    this.setState({
                                                                        totalReductionAmount: parseFloat(this.state.totalReductionAmount - this.state.orderDetails.prices.delivery).toFixed(2),
                                                                        totalRefundAmount: parseFloat(this.state.orderDetails.orderValue - refundAmount).toFixed(2)
                                                                    })
                                                                } else {
                                                                    this.setState({
                                                                        totalReductionAmount: 0,
                                                                        totalRefundAmount: parseFloat(this.state.orderDetails.orderValue - refundAmount).toFixed(2)
                                                                    })
                                                                }

                                                            }
                                                        }, 10);


                                                    }} /></Col>
                                            </Row>
                                            <Row className="px-5">
                                                <div className="border-top w-100">
                                                    <Row>
                                                        <Col md="5" className="text-right">Total refund amount:</Col>
                                                        <Col md="7" className="text-left">{this.state.totalRefundAmount}</Col>
                                                    </Row>
                                                </div>
                                            </Row>
                                            <Row className="px-5">
                                                <Col md="5" className="text-right">Total reduction:</Col>
                                                <Col md="7" className="text-left">{this.state.totalReductionAmount}</Col>
                                            </Row>
                                        </div>
                                    )}
                                </Col>
                            </Row>
                        </ModalBody>
                        <ModalFooter>
                            <Button className="cursor" onClick={this.cancelOrderModal}>Close</Button>{' '}
                            <Button className="cursor" color="primary" onSubmit={(e) => this.cancelOrder(e)}>Cancel Order</Button>
                        </ModalFooter>
                    </form>
                </Modal>


                <Modal isOpen={this.state.showExpDatesModal} className="customPopUp">
                    <ModalHeader className="position-relative" toggle={this.toggle}>Expiry Details of {expiryProductName[this.state.expiryProductIndex]}
                        <Button className="closeButton" onClick={this.showExpDatesModal}>&times;</Button>{' '}
                    </ModalHeader>
                    <ModalBody className="py-0 px-3">
                        <Table id="customTables" className="table table-striped table-bordered mb-5 expiryDetails customTable" responsive>
                            <thead>
                                <tr>
                                    <th>Manufacture Date</th>
                                    <th>Expiry Date</th>
                                    <th>Months Left</th>
                                </tr>
                            </thead>
                            {exptablebody_content[this.state.expiryProductIndex]}
                        </Table>
                    </ModalBody>
                    <ModalFooter className="expiryDetailsFoot">
                        <Button onClick={this.showExpDatesModal}>Close</Button>{' '}
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.deliveryModal} deliveryModal={this.deliveryModal} className='logoutPopup'>
                    <ModalHeader className="position-relative">Confirm <button className="modalClose close" onClick={this.deliveryModal} type="button" aria-label="Close">
                        <span aria-hidden="true"></span>
                    </button></ModalHeader>
                    <ModalBody>
                        <p className="text-center mb-0">Are you sure you want to process the delivery? </p>
                    </ModalBody>
                    <ModalFooter>
                        <Button onClick={() => { this.deliveryOrder(); }}>Yes</Button>
                        <Button color="primary" onClick={() => { this.deliveryModal(); }}>Cancel</Button>
                    </ModalFooter>
                </Modal>

            </div >
        )
    }
}

export default OrderDetails;