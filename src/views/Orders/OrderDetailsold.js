import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Badge, Col, Nav, NavItem, NavLink, Row, TabContent, TabPane, Table, Button, Card, CardBody, CardHeader, Pagination, PaginationItem, PaginationLink, InputGroup, InputGroupAddon, Input, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import queryString from 'query-string'

class OrderDetails extends Component {
    orderId = '';
    authToken = '';
    constructor(props) {
        super(props);
        let userData = localStorage.getItem('userData');
        if(userData !== undefined && userData !== null){
            let authStr = JSON.parse(userData);
            this.authToken = authStr.auth_token;        
        }
        
        
        
        this.changeOrderModal = this.changeOrderModal.bind(this);
        this.cancelOrderModal = this.cancelOrderModal.bind(this);
        this.toggle = this.toggle.bind(this);
        this.orderDetailPage = this.orderDetailPage.bind(this);
        this.state = {
            changeOrderModal: false,
            activeTab: new Array(4).fill('1'),
            dropdownOpen: false,
            cancelOrderModal: false,
            orderStatus: [
                '',
                'NewOrder',
                'Packaging',
                'Dispatched',
                'OnTheWay',
                'Delivered',
                'Verified',
                'CancelledByCustomer',
                'RejectedByShop'
            ],
            orderDetails: {
            },
            shopContactDetails: {
                "address":{
                    "street":"JVL plaza street modified",
                    "zipcode":600018,
                    "location":{
                        "_id":"5bcf15608255d417c0900ea7",
                        "name":"Chennai",
                        "state":"Tamilnadu",
                        "state_code":"TN",
                        "country":"India",
                        "country_code":"IND",
                        "flag":"https://api/4bh3hj24bk2j34.jpg",
                        "settings":"will be added in the future"
                    }
                },
                "name":"Ramesh store",
                "mobile":{
                    "primary":{
                        "dialing_code":91,
                        "number":9962840419,
                        "otp":965509,
                        "is_verified":true
                    },
                    "secondary":{
                        "dialing_code":91,
                        "number":9962840420,
                        "otp":965509,
                        "is_verified":true
                    }
                }               
            },
            customerContactDetails:{                
                "mobile":{
                    "dialing_code":91,
                    "number":8489674524
                },
                "name":"Gunaseelan Subburam"                
            },
            deliveryContactDetails:{
                "location":{
                    "type":"Point",
                    "coordinates":[
                        80.245361328125,
                        13.035489082336426
                    ]
                },
                "address":{
                    "street":"Anna Salai",
                    "area":"Teynampet, Chennai, Tamil Nadu, India",
                    "zipcode":600035
                }
            },


            appliedOfferAmount: 0
        }        
    }

    componentDidMount() {
        const queryParams = queryString.parse(this.props.location.search);
        if(queryParams.order_id !== undefined && this.authToken !== ''){
            axios.get('https://devsa.shoporservice.com/admin/order/'+queryParams.order_id+'/info?text=johns', { 'headers': { 'Authorization': 'JWT '+this.authToken } }).then((response) => { 
                if(response.data !== ''){
                    this.setState({
                        orderDetails: response.data.data
                    });

                    // order value
                    if(this.state.orderDetails.prices.delivery !== undefined){
                        let order_value = Object.assign({}, this.state.orderDetails);
                        order_value.orderValue = order_value.prices.delivery + order_value.prices.net_price;
                        this.setState({
                            orderDetails: order_value
                        });   
                    } else {
                        let order_value = Object.assign({}, this.state.orderDetails);
                        order_value.orderValue = order_value.delivery;
                        this.setState({
                            orderDetails: order_value
                        });
                    }
                    
                    // payment_fee will come under prices only if it is a online payment order.
                    if(this.state.orderDetails.prices.payment_fee == undefined){
                        let payment_fee = Object.assign({}, this.state.orderDetails);
                        payment_fee.prices.payment_fee = 0;
                        this.setState({
                            orderDetails: payment_fee
                        });                     
                    }

                    // If anytime = true, then custom_time will not be present
                    if(this.state.orderDetails.preferences.custom_time == undefined){
                        let custom_time = Object.assign({}, this.state.orderDetails);
                        
                    //     custom_time.preferences.custom_time = 'Immediate';
                    //   alert(custom_time.preferences.custom_time);
                        
                    //     this.setState({
                    //         orderDetails: custom_time
                    //     }); 
                                       
                    }
                }
            }, error => {                 
                return error;
            });



            axios.get('https://devsa.shoporservice.com/admin/order/'+queryParams.order_id+'/support/contact/details', { 'headers': { 'Authorization': 'JWT '+this.authToken } }).then((response) => { 
                if(response.data !== ''){
                    this.setState({
                        shopContactDetails: response.data.data.shop,
                        customerContactDetails: response.data.data.customer,
                        deliveryContactDetails: response.data.data.delivery
                    });                    
                }
            }, error => {                 
                return error;
            });
        }
     }

    changeOrderModal() {
        this.setState({
            changeOrderModal: !this.state.changeOrderModal,
        });
    }

    cancelOrderModal() {
        this.setState({
            cancelOrderModal: !this.state.cancelOrderModal,
        });
    }

    toggle() {
        this.setState(prevState => ({
            dropdownOpen: !prevState.dropdownOpen
        }));
    }


    toggle(tabPane, tab, statustab) {
        const newArray = this.state.activeTab.slice()
        newArray[tabPane] = tab
        this.setState({
            activeTab: newArray,
        });

        localStorage.setItem('setActiveTab', statustab);
        this.props.history.push('/orders')
    }

    orderDetailPage() {
        this.props.history.push('/orderdetails')
    }

    tabPane() {
        return (
            <>
                <div className="mb-4 d-flex flex-row flex-wrap">
                    <div className="twoColLeft col-sm-12 px-0">
                        <TabPane tabId="1">


                            <div className="orderDetailPage mb-4">
                                <div>
                                    <h3 className="text-center pt-3">Order Details</h3>
                                    <div className="col-sm-12 d-flex flex-md-row flex-column align-items-center py-4 tableHeader">
                                        <div className="col-sm-4 mr-4 d-flex mb-md-0 mb-3 flex-row flex-wrap align-items-center">
                                            <InputGroup className="position-relative searchBox w-auto">
                                                <Input className="pl-4" placeholder="Search product" />
                                            </InputGroup>
                                            <span className="w-auto"><i className="fa fa-search ml-2" aria-hidden="true"></i></span>
                                        </div>
                                        <div className="col-sm-4 d-flex flex-row mb-md-0 mb-3 flex-wrap align-items-center mr-4">
                                            <label className="font-weight-bold mb-0 pr-3">Delivery Mode:</label>
                                            <span className="text-success font-weight-bold">{this.state.orderDetails.delivery_type.name}</span>
                                        </div>
                                        <div className="col-sm-4 d-flex flex-row flex-wrap align-items-center mr-4">
                                            <label className="font-weight-bold mb-0 pr-3">Order Status:</label>
                                            <span className="text-primary font-weight-bold">{this.state.orderStatus[this.state.orderDetails.status]}</span>
                                        </div>
                                    </div>
                                </div>

                                <div className="px-3">
                                    <Table id="customTable" className="table table-striped table-bordered customTable" responsive>
                                        <thead>
                                            <tr>
                                                <th>SNo</th>
                                                <th>Product Name</th>
                                                <th>UPC</th>
                                                <th>Quantity</th>
                                                <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.orderDetails.products.map((data, index) =>
                                                <tr key={index}>
                                                    <td>{index + 1}</td>
                                                    <td>{data.product_name}</td>
                                                    <td>{data.upc}</td>
                                                    <td>{data.qty}</td>
                                                    <td>{data.amount}</td>
                                                </tr>
                                            )}
                                        </tbody>
                                    </Table>
                                </div>

                            </div>
                            <Row className="orderDetailsData pb-4">
                                <Col className="font-weight-bold" xs="12" md="4">
                                    <div>
                                        <label className="mb-0 text-right">Applied Offer amount:</label><span>{this.state.appliedOfferAmount}</span>
                                    </div>
                                    <div>
                                        <label className="mb-0 text-right">Total products:</label><span>{this.state.orderDetails.total_products}</span>
                                    </div>
                                    <div>
                                        <label className="mb-0 text-right">Amount paid by customer:</label><span>{this.state.orderDetails.prices.net_price}</span>
                                    </div>
                                    <div>
                                        <label className="mb-0 text-right">Tax if any:</label><span>{this.state.orderDetails.prices.taxes}</span>
                                    </div>
                                </Col>
                                <Col className="font-weight-bold" xs="12" md="5">
                                    <div>
                                        <label className="mb-0 text-right">Card payment fee %:</label><span>{this.state.orderDetails.prices.payment_fee}</span>
                                    </div>
                                    <div>
                                        <label className="mb-0 text-right">UPC commission cost %:</label><span>{this.state.orderDetails.upc_commission_cost}</span>
                                    </div>
                                    <div>
                                        <label className="mb-0 text-right">SKU commission cost %:</label><span>{this.state.orderDetails.sku_commission_cost}</span>
                                    </div>
                                    <div>
                                        <label className="mb-0 text-right">Amount to be paid to merchant:</label><span>{this.state.orderDetails.merchantAmount}</span>
                                    </div>
                                </Col>
                                <Col className="font-weight-bold" xs="12" md="3">
                                    <div>
                                        <label className="mb-0 text-right">Delivery Charge:</label><span>{this.state.orderDetails.prices.delivery}</span>
                                    </div>
                                    <div>
                                        <label className="mb-0 text-right">Delivery Mode:</label><span>{this.state.orderDetails.delivery_type.name}</span>
                                    </div>
                                    <div>
                                        <label className="mb-0 text-right">Payment Mode:</label><span>{this.state.orderDetails.payment_type.name}</span>
                                    </div>
                                </Col>
                                <Col className="font-weight-bold" xs="12" md="12">
                                    <Row className="align-items-center">
                                        <Col xs="12" className="text-right" md="9">
                                            <div>Customer Preferred Delivery time & date: 
                                                {/* <span className="text-primary">{this.state.orderDetails.preferences.custom_time.to}</span> */}
                                            </div>
                                        </Col>
                                        <Col xs="12" className="text-sm-right" md="3"><Button className="text-primary">Assign to delivery boy</Button></Col>
                                    </Row>
                                    <Row className="align-items-center">
                                        <Col xs="12" className="text-right" md="9">
                                            <div>Expected Delivery time:</div>
                                            <span className="text-primary">{this.state.orderDetails.preferences.expected_time.to}</span>
                                        </Col>
                                        <Col xs="12" className="text-right" md="3">Order Value: {this.state.orderDetails.orderValue}</Col>
                                    </Row>
                                   
                                </Col>
                                <Col xs="12" md="12" className="p-3">
                                        <h5>Support Log</h5>
                                        <div className="border supportLog p-3">
                                                <div className="logTitle">
                                                    <span className="pl-0">Ratheesh:</span><span className="pl-0">20/03/2019 - 10:15am</span>
                                                </div>
                                                <div className="logContent">
                                                    <p>Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.</p>
                                                </div>
                                        </div>
                                    </Col>
                                <Col xs="12" md="12" className="px-0 py-3">
                                    <Row className="align-items-center">
                                        <Col xs="12" className="text-center px-0" lg="7">
                                            <Button className="mr-3">View log</Button>
                                        </Col>
                                        <Col xs="12" className="text-lg-right text-center mt-3 px-0" lg="5">
                                            <Button onClick={this.cancelOrderModal} className="mr-3">Cancel Order</Button>
                                            <Button onClick={this.changeOrderModal} className="mr-3">Change Order</Button>
                                            <Link to="/orders"><Button color="primary">Back</Button></Link>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>

                            <Modal isOpen={this.state.changeOrderModal} changeOrderModal={this.changeOrderModal} className={this.props.className}>
                                <ModalHeader className="position-relative" toggle={this.toggle}>Change Order <button className="modalClose close" onClick={this.changeOrderModal} type="button" aria-label="Close">
                                    <span aria-hidden="true"></span>
                                </button></ModalHeader>
                                <ModalBody className="p-0">
                                    <Row className="border-bottom py-2">
                                        <Col xs="12" md="4">
                                            <Row>Shop name:Shaan Mart</Row>
                                            <Row>Purchase cost:200.0</Row>
                                        </Col>
                                        <Col xs="12" md="3">Loss:-20.0</Col>
                                        <Col xs="12" md="3">Actual cost</Col>
                                        <Col xs="12" md="2"><input name="changeOrder" type="radio" /></Col>
                                    </Row>
                                    <Row className="py-2">
                                        <Col xs="12" md="4">
                                            <Row>Shop name:Shaan Mart</Row>
                                            <Row>Purchase cost:200.0</Row>
                                        </Col>
                                        <Col xs="12" md="3">Loss:-20.0</Col>
                                        <Col xs="12" md="3">Actual cost</Col>
                                        <Col xs="12" md="2"><input name="changeOrder" type="radio" /></Col>
                                    </Row>
                                </ModalBody>
                                <ModalFooter>
                                    <Button onClick={this.changeOrderModal}>Close</Button>{' '}
                                    <Button color="primary" onClick={this.changeOrderModal}>Change Order</Button>
                                </ModalFooter>
                            </Modal>


                            <Modal isOpen={this.state.cancelOrderModal} cancelOrderModal={this.cancelOrderModal} className={this.props.className}>
                                <ModalHeader className="position-relative" toggle={this.toggle}>Cancel Order <button className="modalClose close" onClick={this.cancelOrderModal} type="button" aria-label="Close">
                                    <span aria-hidden="true"></span>
                                </button></ModalHeader>
                                <ModalBody className="p-0">
                                    <Row className="align-items-center">
                                        <Col xs="12" className="text-center px-0" md="12">
                                                <Row>
                                                    <Col md="5" className="text-right">Order Value</Col>
                                                    <Col md="7" className="text-left">500.0</Col>
                                                </Row>
                                                <Row>
                                                    <Col md="5" className="text-right">Reduction (%):</Col>
                                                    <Col md="7" className="text-left"><select>
                                                        <option>0%</option>
                                                        <option>50%</option>
                                                        <option>100%</option>
                                                        </select>
                                                        <span>OR</span>    
                                                        <input type="text" placeholder="Value" />
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col md="5" className="text-right">Excluded Delivery Charge:</Col>
                                                    <Col md="7" className="text-left">50.0 <input type="checkbox" /></Col>
                                                </Row>
                                                <Row>
                                                    <Col md="5" className="text-right">Total refund amount</Col>
                                                    <Col md="7" className="text-left">500.0</Col>
                                                </Row>
                                        </Col>
                                    </Row>
                                </ModalBody>
                                <ModalFooter>
                                    <Button onClick={this.cancelOrderModal}>Close</Button>{' '}
                                    <Button color="primary" onClick={this.cancelOrderModal}>Change Order</Button>
                                </ModalFooter>
                            </Modal>

                        </TabPane>
                    </div>



                </div>
            </>
        );

    }

    render() {
        return (
            <div className="animated fadeIn">
                <Row className="flex-row">
                    <Col xs="12" lg="9" className="mb-4 px-0 pt-4 pl-0 pl-lg-3">
                        <Nav className="customTabs" tabs>
                            <NavItem>
                                <NavLink
                                    active={this.state.activeTab[0] === '1'}
                                    onClick={() => { this.toggle(0, '1', 1); }}
                                >
                                    New Order(2)
                    </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    active={this.state.activeTab[0] === '2'}
                                    onClick={() => { this.toggle(0, '2', 2); }}
                                >
                                    Packaging(2)
                      <span className="tabsDataCount">6</span>
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    active={this.state.activeTab[0] === '3'}
                                    onClick={() => { this.toggle(0, '3', 3); }}
                                >
                                    Dispatched(4)
                    </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    active={this.state.activeTab[0] === '4'}
                                    onClick={() => { this.toggle(0, '4', 4); }}
                                >
                                    Shipping(6)
                    </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    active={this.state.activeTab[0] === '5'}
                                    onClick={() => { this.toggle(0, '5', 5); }}
                                >
                                    Delivered(5)
                    </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    active={this.state.activeTab[0] === '6'}
                                    onClick={() => { this.toggle(0, '6', 7); }}
                                >
                                    Cancelled(5)
                    </NavLink>
                            </NavItem>
                        </Nav>
                        <TabContent className="customTabContent" activeTab={this.state.activeTab[0]}>
                            {this.tabPane()}
                        </TabContent>
                    </Col>
                    <Col lg="3" className="px-0 pl-md-3">
                        <div className="twoColRight p-3 h-100">
                            <div className="contentBox mb-3 px-3 py-2 rounded">
                                <p className="mb-2">Shop contact details</p>
                                <p>Name: {this.state.shopContactDetails.name}</p>
                                <p>Mobile number: +{this.state.shopContactDetails.mobile.primary.dialing_code} {this.state.shopContactDetails.mobile.primary.number}</p>
                                <p>Landline: +{this.state.shopContactDetails.mobile.secondary.dialing_code} {this.state.shopContactDetails.mobile.secondary.number}</p>   
                                <p>Location: {this.state.shopContactDetails.address.location.name}</p>
                            </div>
                            <div className="contentBox mt-3 px-3 py-2 rounded">
                                <p className="mb-2">Customer details</p>
                                <p>Name: {this.state.customerContactDetails.name}</p>
                                <p>Mobile number: +{this.state.customerContactDetails.mobile.dialing_code} {this.state.customerContactDetails.mobile.number}</p>
                            </div>

                            <div className="contentBox mt-3 px-3 py-2 rounded">
                                <p className="mb-2">Delivery Details</p>
                                <p>Name: {this.state.customerContactDetails.name}</p>
                                <p>Mobile number: +{this.state.customerContactDetails.mobile.dialing_code} {this.state.customerContactDetails.mobile.number}</p>
                            </div>

                            <div className="contentBox mt-3 px-3 py-2 rounded">
                                <p className="mb-2">Support executives</p>
                                <p>Status: Online</p>
                                <p>Name: Ratheesh</p>
                                <p>Login time: 12:12pm</p>
                                <p>Logout time: inprogress</p>
                                <p className="mt-2">Status: Online</p>
                                <p>Name: Shan</p>
                                <p>Login time: 12:12pm</p>
                                <p>Logout time: inprogress</p>
                                <p className="mt-2">Status: Offline</p>
                                <p>Name: John</p>
                                <p>Login time: 12:12pm</p>
                                <p>Logout time: inprogress</p>
                            </div>

                        </div>
                    </Col>

                </Row>
            </div>
        )
    }
}

export default withRouter(OrderDetails);