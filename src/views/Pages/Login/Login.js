import React, { Component } from 'react';
import axios from 'axios';
import { withRouter } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class Login extends Component {
  constructor(props){
    super(props);
    this.state = {
      "email_id": '',
      "password": ''
    }
    if(localStorage.getItem('userData') !== null && localStorage.getItem('userData') !== undefined){
      this.props.history.push('/orders');
    }
    
  }

  loginSubmit(event){
    event.preventDefault();
    axios.post(process.env.REACT_APP_API_BASE_URL+'/auth', this.state).then((response) => {
      if(response.data.success){
        toast.success("Login Successfull");
        localStorage.setItem("userData", JSON.stringify(response.data.data));
        this.props.history.push('/orders');
      } 
    }, error => {
      if (error.response.status === 401) {
        localStorage.removeItem("userData");
        toast.error(error.response.data.message);
      }
      return error;
    });
  }

  render() {
    return (      
      <div className="app flex-row align-items-center">
        <ToastContainer autoClose={3000} />
        <Container>
          <Row className="justify-content-center">
            <Col className="col-12 col-md-6">
              <CardGroup>
                <Card className="p-4 mx-md-5">
                  <CardBody>
                    <Form onSubmit={(e) => this.loginSubmit(e)}>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" value={this.state.email_id} onChange={e=>{
                          this.setState({email_id: e.target.value}) }} 
                          placeholder="Username" name="email_id" id="email_id" required autoComplete="username" />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" value={this.state.password} onChange={e => {
                          this.setState({password : e.target.value})}} 
                          placeholder="Password" name="password" id="password" required autoComplete="current-password" />
                      </InputGroup>
                      <Row>
                        <Col className="text-center text-md-left" md="6">
                          {/* <Link to="/orders"><Button color="primary" className="px-4" onSubmit={(e)=>this.loginSubmit(e)}>Login</Button></Link> */}
                          <Button color="primary" className="px-4" onSubmit={(e)=>this.loginSubmit(e)}>Login</Button>
                        </Col>
                        <Col className="text-center text-md-center" md="6">
                          <Button color="link" className="px-0">Forgot password?</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default withRouter(Login);
