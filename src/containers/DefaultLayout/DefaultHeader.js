import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Nav, NavItem, Button, DropdownToggle, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import PropTypes from 'prop-types';

import { AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logo from '../../assets/img/brand/logo.png'
import sygnet from '../../assets/img/brand/logo.png';
import { withRouter } from 'react-router-dom';

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  constructor(props) {
    // order value        
    super(props);

    /** Check User is Logged In. Otherwise Redirect to Login Page */
    if(localStorage.getItem('userData') === null || localStorage.getItem('userData') === undefined){
      this.props.history.push('/login');
    }
    this.logoutModal = this.logoutModal.bind(this);

    this.state = {
      logoutModal: false,
      activeTab: '',
    }
  }

  componentDidMount(){
    let order_value = Object.assign({}, this.state.activeTab);  
    order_value = localStorage.getItem('activeTabName')
    this.setState({
      activeTab: order_value
    });  
  }

  /* Logout Confirmation Popup */
  logoutModal() {
    this.setState({
      logoutModal: !this.state.logoutModal,
    });
  }

  /* Logout From current User and Redirect to Login Page */
  logout(){
    localStorage.clear()
    this.props.history.push('/login');
  }

  /* Logout Modal Popup Show and Hide */
  toggle() {
    this.setState(prevState => ({
      logoutModal: !this.state.logoutModal
    }));
  }

  render() {
    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppSidebarToggler className="d-md-down-none" display="lg" />
        <AppNavbarBrand className="siteLogo"
          full={{ src: logo, width: 89, height: 25, alt: 'CoreUI Logo' }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: 'CoreUI Logo' }}
        />

        <Nav className="d-md-down-none headerButtons" navbar>
          <NavItem>
            <Link to="/" className={this.props.activeTab === 'dashboard' ? 'btn btn-secondary active' : 'btn btn-secondary'} >Dashboard</Link>
          </NavItem>
          {/* <NavItem>
            <Link to="/" className="btn btn-secondary" >Customers</Link>
          </NavItem>
          <NavItem>
            <Link to="/" className="btn btn-secondary" >Finance</Link>
          </NavItem>
          <NavItem>
            <Link to="/" className="btn btn-secondary" >Merchants</Link>
          </NavItem>
          <NavItem>
            <Link to="/" className="btn btn-secondary" >Shops</Link>
          </NavItem>
          <NavItem>
            <Link to="/" className="btn btn-secondary" >Billings</Link>
          </NavItem> */}
          <NavItem>
            <Link to="/orders" className={this.props.activeTab === 'orders' ? 'btn btn-secondary active' : 'btn btn-secondary'} >Orders</Link>
          </NavItem>
          {/* <NavItem>
            <Link to="/" className="btn btn-secondary" >Admin</Link>
          </NavItem>
          <NavItem>
            <Link to="/" className="btn btn-secondary" >Messages</Link>
          </NavItem> */}
        </Nav>
        <Nav className="ml-auto" navbar>

          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
              <img src={'../../assets/img/avatars/6.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com" />
            </DropdownToggle>
          </AppHeaderDropdown>
        </Nav>

        <Nav className="d-md-down-none headerButtons ml-3" navbar>
          <NavItem>
            <Button className="logOutButton btn btn-secondary cursor" onClick={this.logoutModal}> <i className="fa fa-sign-out" aria-hidden="true"></i> Logout</Button>
          </NavItem>
        </Nav>

        <Modal isOpen={this.state.logoutModal} logoutModal={this.logoutModal} className='logoutPopup'>
          <ModalHeader className="position-relative">Confirm <button className="modalClose close" onClick={this.logoutModal} type="button" aria-label="Close">
            <span aria-hidden="true"></span>
          </button></ModalHeader>
          <ModalBody>
            <p className="text-center mb-0">Are you Sure. you want to logout? </p>
          </ModalBody>
          <ModalFooter>
            <Button onClick={() => { this.logout(); }}>Yes</Button>
            <Button color="primary" onClick={() => { this.toggle(); }}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default withRouter(DefaultHeader);
